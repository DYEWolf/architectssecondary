import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceStorageService {

  constructor() { }

  // Methods of the user component

  setSingleUser(user: any) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  getSingleUser() {
    return localStorage.getItem('user');
  }

  deleteSingleUser() {
    localStorage.removeItem('user');
  }

  setRoleUser(role: string) {
    localStorage.setItem('role', role);
  }

  getRoleUser() {
    return localStorage.getItem('role');
  }

  deleteRoleUser() {
    localStorage.removeItem('role');
  }

  setAssignedTo(assignedTo: string) {
    localStorage.setItem('assignedTo', assignedTo);
  }

  getAssignedTo() {
    return localStorage.getItem('assignedTo');
  }

  deleteAssignedTo() {
    localStorage.removeItem('assignedTo');
  }

  setWebsafeKey(websafeKey: string) {
    localStorage.setItem('websafeKey', websafeKey);
  }

  getWebsafeKey() {
    return localStorage.getItem('websafeKey');
  }

  deleteWebsafeKey() {
    localStorage.removeItem('websafeKey');
  }
  // Methods of the structure component

  setSingleStructure(structure: any) {
    localStorage.setItem('structure', JSON.stringify(structure));
  }

  getSingleStructure() {
    return localStorage.getItem('structure');
  }

  deleteSingleStructure() {
    localStorage.removeItem('structure');
  }

  // Methods to the current user session

  setUserSession(userSession: any) {
    localStorage.setItem('userSession', JSON.stringify(userSession));
  }

  getUserSession() {
    return localStorage.getItem('userSession');
  }

  deleteUserSession() {
    localStorage.removeItem('userSession');
  }

  // Methods of the clients component

  setSingleClient(client: any) {
    localStorage.setItem('client', JSON.stringify(client));
  }

  getSingleClient() {
    return localStorage.getItem('client');
  }

  deleteSingleClient() {
    localStorage.removeItem('client');
  }

  // Methods of the service order component

  setCylinderArray(cylinderArray: any) {
      localStorage.setItem('cylinderArray', JSON.stringify(cylinderArray));
  }

  getCylinderArray() {
    return localStorage.getItem('cylinderArray');
  }

  deleteCylinderArray() {
    localStorage.removeItem('cylinderArray');
  }

  setBeamArray(beamArray: any) {
    localStorage.setItem('beamArray', JSON.stringify(beamArray));
  }

  getBeamArray() {
    return localStorage.getItem('beamArray');
  }

  deleteBeamArray() {
    localStorage.removeItem('beamArray');
  }

  setCubeArray(cubeArray: any) {
    localStorage.setItem('cubeArray', JSON.stringify(cubeArray));
  }

  getCubeArray() {
    return localStorage.getItem('cubeArray');
  }

  deleteCubeArray() {
    localStorage.removeItem('cubeArray');
  }

  storeServiceOrder(serviceOrder: any) {
    localStorage.setItem('serviceOrder', JSON.stringify(serviceOrder));
  }

  getServiceOrder() {
    return localStorage.getItem('serviceOrder');
  }

  deleteServiceOrder() {
    localStorage.removeItem('serviceOrder');
  }

  deleteAll() {
    this.deleteCylinderArray();
    this.deleteBeamArray();
    this.deleteCubeArray();
    this.deleteServiceOrder();
    this.deleteSingleUser();
    this.deleteSingleClient();
    this.deleteSingleStructure();
    this.deleteUserSession();
    this.deleteRoleUser();
    this.deleteAssignedTo();
  }

  deleteServiceOrderInfo() {
    this.deleteCylinderArray();
    this.deleteBeamArray();
    this.deleteCubeArray();
    this.deleteServiceOrder();
  }


}
