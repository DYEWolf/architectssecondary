export class Employee {
  name: string;
  email: string;
  userName: string;
  password: string;
  confirmPassword: string;
  role: string;
  profileImage: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
