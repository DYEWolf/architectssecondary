export class Beams {
  colado: number = 0;
  ruptura: number = 0;
  edad: number = 0;
  rev: number = 0;
  ancho: number = 0;
  peralte: number = 0;
  distanciaEntreApoyos: number = 0;
  cargaKn: number = 0;
  cargaKgf: number = 0;
  mr: number = 0;
  resistenciaMpa: number = 0;
  mrProy: number = 0;
  resistenciaPorce: number = 0;
  ubicacion: any;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
