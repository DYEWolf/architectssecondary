export class Cubes {
  colado: number = 0;
  ruptura: number = 0;
  edad: number = 0;
  rev: number = 0;
  largo: number = 0;
  ancho: number = 0;
  area: number = 0;
  cargaKn: number = 0;
  cargaKgf: number = 0;
  resistenciaKgf: number = 0;
  resistenciaMpa: number = 0;
  resistenciaProy: number = 0;
  resistenciaPorce: number = 0;
  ubicacion: any;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
