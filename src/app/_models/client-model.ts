export class Clients {
  name: string;
  address: string;
  extNumber: string;
  intNumber: string;
  suburb: string;
  locality: string;
  city: string;
  state: string;
  zipCode: string;
  rfc: string;
  email: string;
  phoneNumber: string;
  contact: string;
}
