export class Structures {
  address: string;
  businessName: string;
  companyRef: string;
  contact: string;
  name: string;
  phoneNumber: number;
  userEmail: string;
  taxDomicile: string;
}
