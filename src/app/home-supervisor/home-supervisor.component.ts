import { Component, OnInit } from '@angular/core';
import { ServiceStorageService } from './../_storage/service-storage.service';


import { UserService } from './../_services/user.service';

@Component({
  selector: 'app-home-supervisor',
  templateUrl: './home-supervisor.component.html',
  styleUrls: ['./home-supervisor.component.css']
})
export class HomeSupervisorComponent implements OnInit {

  userName;

  constructor(
    private serviceStorage: ServiceStorageService
  ) {}

   ngOnInit() {
     const user = JSON.parse(this.serviceStorage.getUserSession());
     console.log(user);
     this.userName = user.name.toUpperCase();
   }

}
