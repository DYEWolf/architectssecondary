import { ServiceStorageService } from './../../_storage/service-storage.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import { Clients } from './../../_models/client-model';
import { ClientService } from './../../_services/clients.service';
import { UserService } from '../../_services/user.service';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  errMessage: string;
  role: string;
  clientForm: FormGroup;
  usersafeKey: string;
  userName = 'Seleccionar';
  users: any = [];

  constructor(
    private clientService: ClientService,
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private serviceStorage: ServiceStorageService,
  ) {}

  ngOnInit() {
    const role = 'Supervisor';
    this.role = this.serviceStorage.getRoleUser();
    this.clientForm = this.fb.group({
      name: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      number: new FormControl(''),
      numberExt: new FormControl('', Validators.required),
      suburb: new FormControl('', Validators.required),
      locality: new FormControl(''),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      zipCode: new FormControl('', [Validators.required, Validators.max(99999)]),
      rfc: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
      contact: new FormControl('', Validators.required)
    });
    this.userService.getAllUsersByRole(role).subscribe(response => {
      this.users = response;
    });
  }

  save() {
    if (this.userName === 'Seleccionar') {
      this.errMessage = 'Por favor asigne la obra';
    } else {

    const client = this.clientForm.value;
    const email = client.email;
    client.email = {email};
    this.clientService.postClient(client, this.usersafeKey).subscribe(res => {

      if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/clients']);
      } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/clients']);
      }
    }, err => {
      this.errMessage = 'Error al agregar cliente';

    });
    }
  }


  cancel() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/clients']);
    } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/clients']);
    }
  }

  userInfo(name: string, usersafeKey: string) {
    this.errMessage = null;
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }

}
