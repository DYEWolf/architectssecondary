import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../../_services/user.service';
import { ServiceStorageService } from '../../_storage/service-storage.service';
import { ClientService } from './../../_services/clients.service';
import { Clients } from './../../_models/client-model';


@Component({
  selector: 'app-clients-menu',
  templateUrl: './clients-menu.component.html',
  styleUrls: ['./clients-menu.component.css']
})
export class ClientsMenuComponent implements OnInit {

  modalReference: any;
  clientsModel: Clients;
  role: string;
  clients: any = [];
  websafeKey: string;
  delete = false;
  clientName: string;
  isNextPage: number;
  nextPageToken: string;

  constructor(
    private clientsService: ClientService,
    private modalService: NgbModal,
    private router: Router,
    private userService: UserService,
    private serviceStorage: ServiceStorageService,
  ) {}

  ngOnInit() {
    this.clientsService.getAllClients().subscribe(data => {
      this.clients = data;
      this.nextPageToken = this.clients.nextPageToken;
      this.isNextPage = this.clients.items.length;
    });
    this.role = this.serviceStorage.getRoleUser();
    this.clientsModel = new Clients();
  }

  more(seeMore, i: number) {
    this.clientsModel.address = this.clients.items[i].address;
    this.clientsModel.city = this.clients.items[i].city;
    this.clientsModel.contact = this.clients.items[i].contact;
    this.clientsModel.email = this.clients.items[i].email.email;
    this.clientsModel.extNumber = this.clients.items[i].numberExt;
    this.clientsModel.intNumber = this.clients.items[i].number;
    this.clientsModel.locality = this.clients.items[i].locality;
    this.clientsModel.name = this.clients.items[i].name;
    this.clientsModel.phoneNumber = this.clients.items[i].phoneNumber;
    this.clientsModel.rfc = this.clients.items[i].rfc;
    this.clientsModel.state = this.clients.items[i].state;
    this.clientsModel.suburb = this.clients.items[i].suburb;
    this.clientsModel.zipCode = this.clients.items[i].zipCode;
    this.clientsModel.name = this.clients.items[i].name;
    this.modalReference = this.modalService.open(seeMore);
  }

  moreClients() {
    let newClients: any;
    this.clientsService.getNextPageTokenClients(this.nextPageToken).subscribe(res => {
      newClients = res['items'];
      this.nextPageToken = res['nextPageToken'];
      if (!res['items']) {
        alert('No hay mas elementos que mostrar');
      } else {
        this.clients.items = this.clients.items.concat(newClients);
      }
    });
  }

  add() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/clients/add']);
    } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/clients/add']);
    }
  }

  deleteClient() {
    if (this.delete === false) {
      const suspend = true;
      this.clientsService.deleteClient(this.websafeKey, suspend).subscribe(res => {
        this.modalReference.close();
        location.reload();
     });
    } else {
      const suspend = false;
      this.clientsService.deleteClient(this.websafeKey, suspend).subscribe(res => {
        this.modalReference.close();
        location.reload();
      });
    }
  }

  editClient(i: number) {
    const websafeKey = this.clients.items[i].websafeKey;
    this.clientsService.getSingleClient(websafeKey).subscribe(res => {
      this.serviceStorage.setSingleClient(res);
      if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/clients/edit']);
      } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/clients/edit']);
      }
    });
  }

  open(deleteModal, i: number) {
    this.modalReference = this.modalService.open(deleteModal, {size: 'lg'});
    this.websafeKey = this.clients.items[i].websafeKey;
    this.clientName = this.clients.items[i].name;
  }
}
