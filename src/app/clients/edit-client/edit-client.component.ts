import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import { ClientService } from './../../_services/clients.service';
import { Clients } from './../../_models/client-model';
import { UserService } from '../../_services/user.service';
import { ServiceStorageService } from '../../_storage/service-storage.service';


@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {

  websafeKey: any;
  errMessage: string;
  modelClient: any = {};
  role: string;
  users: any = [];
  userName = 'Seleccionar';

  client: any = [];
  clientForm: FormGroup;
  usersafeKey: string;

  constructor(
    private clientsService: ClientService,
    private router: Router,
    private userService: UserService,
    private serviceStorage: ServiceStorageService,
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    const role = 'Supervisor';
    this.role = this.serviceStorage.getRoleUser();
    this.client = JSON.parse(this.serviceStorage.getSingleClient());
    this.usersafeKey = this.client.assignedTo.websafeKey;
    const assignedTo = this.client.assignedTo.name;
    this.userName = assignedTo;
    this.clientForm = this.fb.group({
      name: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      number: new FormControl(''),
      numberExt: new FormControl('', Validators.required),
      suburb: new FormControl('', Validators.required),
      locality: new FormControl(''),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      zipCode: new FormControl('', [Validators.required, Validators.max(99999)]),
      rfc: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
      contact: new FormControl('', Validators.required)
    });
    this.clientForm.patchValue({
      name: this.client.name,
      address: this.client.address,
      number: this.client.number,
      numberExt: this.client.numberExt,
      suburb: this.client.suburb,
      locality: this.client.locality,
      city: this.client.city,
      state: this.client.state,
      zipCode: this.client.zipCode,
      rfc: this.client.rfc,
      email: this.client.email.email,
      phoneNumber: this.client.phoneNumber,
      contact: this.client.contact
    });
    this.userService.getAllUsersByRole(role).subscribe(response => {
      this.users = response;
    });
  }

  save () {
    const client = this.clientForm.value;
    const websafeKey = this.client.websafeKey;
    const email = client.email;
    client.email = {email};
    this.clientsService.editClient(client, websafeKey, this.usersafeKey).subscribe(res => {
       if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/clients']);
        } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/clients']);
        }
      }, err => {
        this.errMessage = 'Error al agregar cliente';
      });
  }

  cancel() {
    if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/clients']);
      } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/clients']);
      }
  }

  userInfo(name: string, usersafeKey: string) {
    this.errMessage = null;
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }


}
