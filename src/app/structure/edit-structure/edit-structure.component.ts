import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../_services/user.service';
import { ServiceStorageService } from '../../_storage/service-storage.service';
import { ClientService } from './../../_services/clients.service';
import { StructureService } from './../../_services/structures.service';
import { Structures } from './../../_models/structure-model';


@Component({
  selector: 'app-edit-structure',
  templateUrl: './edit-structure.component.html',
  styleUrls: ['./edit-structure.component.css']
})
export class EditStructureComponent implements OnInit {

  clientName: string;
  companyRef: string;
  errMessage: string;
  websafeKey: string;
  role: string;
  structure: any = [];
  clients: any = [];
  structureForm: FormGroup;
  users: any = [];
  userName: string;
  usersafeKey: string;

  constructor(
    private router: Router,
    private structuresService: StructureService,
    private clientService: ClientService,
    private userService: UserService,
    private serviceStorage: ServiceStorageService,
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    const role = 'Supervisor';
      this.role = this.serviceStorage.getRoleUser();
      this.structure = JSON.parse(this.serviceStorage.getSingleStructure());
      this.clientName = this.structure.businessName;
      this.clientService.getAllClients().subscribe(data => {
        const clients = data['items'];
        const clientReference = clients.filter(x => x.name === this.clientName);
        this.companyRef = clientReference[0].websafeKey;
      });
      this.userName = this.structure.assignedTo.name;
      this.websafeKey = this.structure.websafeKey;
      this.usersafeKey = this.structure.assignedTo.websafeKey;
      this.structureForm = this.fb.group({
        address: new FormControl('', Validators.required),
        contact: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        phoneNumber: new FormControl('', Validators.required),
        responsibleStaff: new FormControl('', Validators.required),
        taxDomicile: new FormControl('', Validators.required),
      });
      this.structureForm.patchValue({
        address: this.structure.address,
        contact: this.structure.contact,
        name: this.structure.name,
        phoneNumber: this.structure.phoneNumber,
        responsibleStaff: this.structure.responsibleStaff.email,
        taxDomicile: this.structure.taxDomicile
      });
      this.userService.getAllUsersByRole(role).subscribe(response => {
        this.users = response;
      });
  }

  save() {
      const structure = this.structureForm.value;
      const email = structure.responsibleStaff;
      structure.responsibleStaff = {email};
      structure.companyRef = this.companyRef;
      structure.businessName = this.clientName;
      this.structuresService.editStructure(structure, this.websafeKey, this.usersafeKey).subscribe(res => {
        if (this.role === 'Admin' || this.role === 'Director') {
          this.router.navigate(['/home/structures']);
          } else if (this.role === 'Supervisor') {
          this.router.navigate(['/home-supervisor/structures']);
          }
      }, err => this.errMessage = 'Error al agregar obra');

  }

  cancel() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/structures']);
      } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/structures']);
      }
    }

    userInfo(name: string, usersafeKey: string) {
      this.errMessage = null;
      this.userName = name;
      this.usersafeKey = usersafeKey;
    }
}
