import { UserService } from './../../_services/user.service';
import { StructureService } from './../../_services/structures.service';
import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { ServiceStorageService } from '../../_storage/service-storage.service';
import { Structures } from './../../_models/structure-model';


@Component({
  selector: 'app-structure-menu',
  templateUrl: './structure-menu.component.html',
  styleUrls: ['./structure-menu.component.css']
})
export class StructureMenuComponent implements OnInit {

  structures: any = [];
  structure: Structures;
  modalReference: any;
  role: string;
  websafeKey: string;
  delete = false;
  structureName: string;
  isNextPage: number;
  nextPageToken: string;

  constructor(
    private structureService: StructureService,
    private modalService: NgbModal,
    private router: Router,
    private userService: UserService,
    private serviceStorage: ServiceStorageService,
  ) {}

  ngOnInit() {
    this.role = this.serviceStorage.getRoleUser();
      this.structureService.getAllStructures().subscribe(res => {
        this.structures = res;
        this.nextPageToken = this.structures.nextPageToken;
        this.isNextPage = this.structures.items.length;
      });
  }

  more(seeMore, i: number) {
   this.structure = new Structures();
   this.structureName = this.structures.items[i].name;
   this.structure.name = this.structures.items[i].name;
   this.structure.businessName = this.structures.items[i].businessName;
   this.structure.address = this.structures.items[i].address;
   this.structure.contact = this.structures.items[i].contact;
   this.structure.userEmail = this.structures.items[i].responsibleStaff.email;
   this.structure.phoneNumber = this.structures.items[i].phoneNumber;
   this.structure.taxDomicile = this.structures.items[i].taxDomicile;
   this.modalReference = this.modalService.open(seeMore);
  }

  moreStructures() {
    let newStructures: any;
    this.structureService.getNextPageTokenStructures(this.nextPageToken).subscribe(res => {
      newStructures = res['items'];
      this.nextPageToken = res['nextPageToken'];
      if (!res['items']) {
        alert('No hay mas elementos que mostrar');
      } else {
        this.structures.items = this.structures.items.concat(newStructures);
      }
    });
  }

  add() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/structures/add']);
      } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/structures/add']);
      }
  }

  editStructure(i: number) {
    const websafekey = this.structures.items[i].websafeKey;
    this.structureService.getSingleStructure(websafekey).subscribe(res => {
      this.serviceStorage.setSingleStructure(res);
      if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/structures/edit']);
        } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/structures/edit']);
        }
    });
  }

  deleteStructure() {
    if (this.delete === false) {
      const suspend = true;
      this.structureService.deleteStructure(this.websafeKey, suspend).subscribe(res => {
        this.modalReference.close();
        location.reload();
      });
    } else {
      const suspend = false;
      this.structureService.deleteStructure(this.websafeKey, suspend).subscribe(res => {
        this.modalReference.close();
        location.reload();
      });
    }
  }

  open(deleteModal, i: number) {
    this.modalReference = this.modalService.open(deleteModal, {size: 'lg'});
    this.websafeKey = this.structures.items[i].websafeKey;
    this.structureName = this.structures.items[i].name;
  }

}
