import { ServiceStorageService } from './../../_storage/service-storage.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../_services/user.service';
import { ClientService } from './../../_services/clients.service';
import { StructureService } from './../../_services/structures.service';
import { Structures } from './../../_models/structure-model';


@Component({
  selector: 'app-add-structure',
  templateUrl: './add-structure.component.html',
  styleUrls: ['./add-structure.component.css']
})
export class AddStructureComponent implements OnInit {

  structureForm: FormGroup;
  clientName = 'Cliente';
  websafeKey: string;
  role: string;
  clients: any = [];
  errMessage: string;
  usersafeKey: string;
  users: any = [];
  userName = 'Seleccionar';


  constructor(
    private router: Router,
    private structuresService: StructureService,
    private clientService: ClientService,
    private userService: UserService,
    private fb: FormBuilder,
    private serviceStorage: ServiceStorageService,
  ) {}

  ngOnInit() {
    const role = 'Supervisor';
    this.role = this.serviceStorage.getRoleUser();
      this.clientService.getAllClients().subscribe(data => {
        this.clients = data;
      });
      this.structureForm = this.fb.group({
        address: new FormControl('', Validators.required),
        contact: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        phoneNumber: new FormControl('', Validators.required),
        responsibleStaff: new FormControl('', Validators.required),
        taxDomicile: new FormControl('', Validators.required),
      });
      this.userService.getAllUsersByRole(role).subscribe(response => {
        this.users = response;
      });
  }

  save() {
    if (this.clientName === 'Cliente') {
      this.errMessage = 'Falta escoger cliente';
    } else {
      const structure = this.structureForm.value;
      const email = structure.responsibleStaff;
      structure.responsibleStaff = {email};
      structure.companyRef = this.websafeKey;
      structure.businessName = this.clientName;
      console.log(structure);
      this.structuresService.postStructure(structure, this.usersafeKey).subscribe(res => {
        if (this.role === 'Admin' || this.role === 'Director') {
          this.router.navigate(['/home/structures']);
          } else if (this.role === 'Supervisor') {
          this.router.navigate(['/home-supervisor/structures']);
          }
      }, err => {
        this.errMessage = 'Error al agregar obra';
    });
    }
  }

  cancel() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/structures']);
      } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/structures']);
      }
  }

  clientInfo(clientName: string, websafeKey: string) {
  this.clientName = clientName;
  this.websafeKey = websafeKey;
  }

  userInfo(name: string, usersafeKey: string) {
    this.errMessage = null;
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }
}
