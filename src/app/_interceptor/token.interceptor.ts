import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import { AuthService } from './../_services/auth.service';
import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor , HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { filter} from 'rxjs/operators';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/switchMap';






@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  isRefreshingToken = false;
  isActiveToken = true;

  constructor(
    private injector: Injector,
    private router: Router,
  ) {}

  addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({headers: new HttpHeaders()
      .set('Authorization', 'Bearer ' + token )
      .set('Content-Type', 'application/json')});
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const auth = this.injector.get(AuthService);
    const authToken = auth.getLocalAccessToken();
    const refreshToken = auth.getLocalRefreshToken();

  // verify if exist a token or not, if not this if is triggered so no header will be applied
    if (authToken == null) {
      return next.handle(request).do((ev: HttpEvent<any>) => {
        if (ev instanceof HttpResponse) {
          console.log('Processing response', ev);
        }
      }).catch(response => {
        if (response instanceof HttpErrorResponse) {
          console.log('Processing http error', response);
        }
        return Observable.throw(response);
      });
    } else if (this.isActiveToken === false && authToken != null) {
      return next.handle(request).do((ev: HttpEvent<any>) => {
        if (ev instanceof HttpResponse) {
          console.log('Processing response', ev);
        }
      }).catch(response => {
        if (response instanceof HttpErrorResponse) {
          console.log('Processing http error', response);
        }
        return Observable.throw(response);
      });
    } else {
        return next.handle(this.addToken(request, auth.getLocalAccessToken())).do((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            console.log('Processing response', event);
          }
        }, (err: any) => {
          if (err instanceof HttpErrorResponse) {
            console.log('Processing http error', err);
            if (err.status === 401) {
              this.isActiveToken = false;
              return this.retry401(request, next);
            } else {
             this.router.navigate(['login']);
            }
          }
        });

      }
    }

    retry401(request: HttpRequest<any>, next: HttpHandler) {
      const auth = this.injector.get(AuthService);
      const authToken = auth.getLocalAccessToken();
      const refreshToken = auth.getLocalRefreshToken();
      if (!this.isRefreshingToken) {
        this.isRefreshingToken = true;
        this.tokenSubject.next(null);
        return auth.refreshToken().subscribe(res => {
          if (res) {
            this.tokenSubject.next(res['accessToken']);
            this.isActiveToken = true;
            console.log(request);
            return next.handle(this.addToken(request, res));
          } else {
            this.router.navigate(['login']);
          }
        }, err => {
          console.log('error: ', err);
        });
      } else {
        return this.tokenSubject
        .filter(token => token != null)
        .take(1)
        .switchMap(token => {
          return next.handle(this.addToken(request, token));
        });
      }
    }

    }
