import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import { AuthService } from './../_services/auth.service';
import { UserService } from './../_services/user.service';
import { ServiceStorageService } from './../_storage/service-storage.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errMessage: string;
  loginForm: FormGroup;

  constructor(
   private authService: AuthService,
   private userService: UserService,
   private router: Router,
   private serviceStorage: ServiceStorageService,
   private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.authService.deleteCredentials();
    this.serviceStorage.deleteAll();
    this.loginForm = this.fb.group({
      user: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
    document.body.classList.add('bg-img');
  }


  login() {
    const login = this.loginForm.value;
    this.authService.getAccesToken(login.user, login.password).subscribe(
      data => {
      this.userService.getUser().subscribe( res => {
        if (res['role'] === 'Admin') {
          this.serviceStorage.setUserSession(res);
          this.serviceStorage.setRoleUser(res['role']);
        } else {
          this.serviceStorage.setUserSession(res);
          this.serviceStorage.setRoleUser(res['role']);
          this.serviceStorage.setAssignedTo(res['assignedTo']['websafeKey']);
          this.serviceStorage.setWebsafeKey(res['websafeKey']);
        }
        if (res['role'] === 'Admin' || res['role'] === 'Director') {
          document.body.classList.remove('bg-img');
          this.router.navigate(['home/service-order']);
        } else if (res['role'] === 'Tecnico') {
          document.body.classList.remove('bg-img');
          this.router.navigate(['home-tecnician/service-order']);
        } else if (res['role'] === 'Supervisor') {
          document.body.classList.remove('bg-img');
          this.router.navigate(['home-supervisor/service-order']);
        }

      }, err => {
        this.errMessage = 'Problema al autenticar, intente de  nuevo';
      });
      }, err => {
        this.errMessage = 'User o Password incorrectos';
      });
  }


}
