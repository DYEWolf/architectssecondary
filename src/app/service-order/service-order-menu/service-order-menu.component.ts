import { UserService } from './../../_services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceOrderService } from '../../_services/service-order.service';
import { ServiceStorageService } from './../../_storage/service-storage.service';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {NgbPaginationConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-service-order-menu',
  templateUrl: './service-order-menu.component.html',
  styleUrls: ['./service-order-menu.component.css']
})
export class ServiceOrderMenuComponent implements OnInit {

  page = 1;
  role: string;
  currentJustify = 'fill';
  delete = false;
  modalReference: any;
  websafeKey: string;
  activeServiceOrder: any = [];
  inProcessServiceOrder: any = [];
  delayedServiceOrder: any = [];
  completedServiceOrder: any = [];
  isNextPageInProcess: number;
  isNextPageActive: number;
  isNextPageDelayed: number;
  isNextPageFinished: number;
  nextPageTokenInProcess: string;
  nextPageTokenActive: string;
  nextPageTokenDelayed: string;
  nextPageTokenFinished: string;

  constructor(
    private userService: UserService,
    private router: Router,
    private serviceOrder: ServiceOrderService,
    private serviceStorage: ServiceStorageService,
    private modalService: NgbModal,
  ) {}

  ngOnInit() {
    this.role = this.serviceStorage.getRoleUser();
    const inProcess = 'En Proceso';
    const delayed = 'Atrasado';
    const active = 'Activo';
    const completed = 'Terminado';
    if (this.role === 'Tecnico') {
      const websafeKey = this.serviceStorage.getWebsafeKey();
      this.serviceOrder.getServiceOrderTecnician(websafeKey, inProcess).subscribe(response  => {
        this.inProcessServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenInProcess = this.inProcessServiceOrder.nextPageToken;
        this.isNextPageInProcess = this.inProcessServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderTecnician(websafeKey, delayed).subscribe(response  => {
        this.delayedServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenDelayed = this.delayedServiceOrder.nextPageToken;
        this.isNextPageDelayed = this.delayedServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderTecnician(websafeKey, active).subscribe(response  => {
        this.activeServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenActive = this.activeServiceOrder.nextPageToken;
        this.isNextPageActive = this.activeServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderTecnician(websafeKey, completed).subscribe(response  => {
        this.completedServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenFinished = this.completedServiceOrder.nextPageToken;
        this.isNextPageFinished = this.completedServiceOrder.items.length;
        }
      });
    } else if (this.role === 'Supervisor') {
      const websafeKey = this.serviceStorage.getWebsafeKey();
      this.serviceOrder.getServiceOrderSupervisor(websafeKey, inProcess).subscribe(response => {
        this.inProcessServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenInProcess = this.inProcessServiceOrder.nextPageToken;
        this.isNextPageInProcess = this.inProcessServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderSupervisor(websafeKey, delayed).subscribe(response => {
        this.delayedServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenDelayed = this.delayedServiceOrder.nextPageToken;
        this.isNextPageDelayed = this.delayedServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderSupervisor(websafeKey, active).subscribe(response => {
        this.activeServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenActive = this.activeServiceOrder.nextPageToken;
        this.isNextPageActive = this.activeServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderSupervisor(websafeKey, completed).subscribe(response => {
        this.completedServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenFinished = this.completedServiceOrder.nextPageToken;
        this.isNextPageFinished = this.completedServiceOrder.items.length;
        }
      });
    } else  {
      this.serviceOrder.getServiceOrderList(inProcess).subscribe(response => {
        this.inProcessServiceOrder = response;
        console.log(response);
        if (response['items']) {
        this.nextPageTokenInProcess = this.inProcessServiceOrder.nextPageToken;
        this.isNextPageInProcess = this.inProcessServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderList(delayed).subscribe(response => {
        this.delayedServiceOrder = response;
        console.log(response);
        if (response['items']) {
        this.nextPageTokenDelayed = this.delayedServiceOrder.nextPageToken;
        this.isNextPageDelayed = this.delayedServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderList(active).subscribe(response => {
        this.activeServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenActive = this.activeServiceOrder.nextPageToken;
        this.isNextPageActive = this.activeServiceOrder.items.length;
        }
      });
      this.serviceOrder.getServiceOrderList(completed).subscribe(response => {
        this.completedServiceOrder = response;
        if (response['items']) {
        this.nextPageTokenFinished = this.completedServiceOrder.nextPageToken;
        this.isNextPageFinished = this.completedServiceOrder.items.length;
        }
      });
    }
  }

  add() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/service-order/add']);
    } else if (this.role === 'Tecnico') {
      this.router.navigate(['/home-tecnician/service-order/add']);
    } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/service-order/add']);
    }
  }

  more(serviceOrder: any) {
  this.serviceStorage.storeServiceOrder(serviceOrder);
  if (this.role === 'Admin' || this.role === 'Director') {
    this.router.navigate(['/home/service-order/edit']);
  } else if (this.role === 'Tecnico') {
    this.router.navigate(['/home-tecnician/service-order/edit']);
  } else if (this.role === 'Supervisor') {
    this.router.navigate(['/home-supervisor/service-order/edit']);
  }
  }

  moreServiceOrderAdmin(type: string) {
    const inProcess = 'En Proceso';
    const delayed = 'Atrasado';
    const active = 'Activo';
    const completed = 'Terminado';
    if (type == 'inProcess') {
      this.serviceOrder.getNextPageServiceOrderAdmin(inProcess, this.nextPageTokenInProcess).subscribe(res => {
        this.nextPageTokenInProcess = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.inProcessServiceOrder.items = this.inProcessServiceOrder.items.concat(res['items']);
        }
      });
    } else if (type == 'active') {
      this.serviceOrder.getNextPageServiceOrderAdmin(active, this.nextPageTokenActive).subscribe(res => {
        this.nextPageTokenActive = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.activeServiceOrder.items = this.activeServiceOrder.items.concat(res['items']);
        }
      });
    } else if (type == 'delayed') {
      this.serviceOrder.getNextPageServiceOrderAdmin(delayed, this.nextPageTokenDelayed).subscribe(res => {
        this.nextPageTokenDelayed = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.delayedServiceOrder.items = this.delayedServiceOrder.items.concat(res['items']);
        }
      });
    } else {
      this.serviceOrder.getNextPageServiceOrderAdmin(completed, this.nextPageTokenFinished).subscribe(res => {
        this.nextPageTokenFinished = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.completedServiceOrder.items = this.completedServiceOrder.items.concat(res['items']);
        }
      });
    }
  }

  moreServiceOrderSupervisor(type: string) {
    const websafeKey = this.serviceStorage.getWebsafeKey();
    const inProcess = 'En Proceso';
    const delayed = 'Atrasado';
    const active = 'Activo';
    const completed = 'Terminado';
    if (type == 'inProcess') {
      this.serviceOrder.getNextPageServiceOrderSupervisor(websafeKey, inProcess, this.nextPageTokenInProcess).subscribe(res => {
        this.nextPageTokenInProcess = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.inProcessServiceOrder.items = this.inProcessServiceOrder.items.concat(res['items']);
        }
      });
    } else if (type == 'active') {
      this.serviceOrder.getNextPageServiceOrderSupervisor(websafeKey, active, this.nextPageTokenActive).subscribe(res => {
        this.nextPageTokenActive = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.activeServiceOrder.items = this.activeServiceOrder.items.concat(res['items']);
        }
      });
    } else if (type == 'delayed') {
      this.serviceOrder.getNextPageServiceOrderSupervisor(websafeKey, delayed, this.nextPageTokenDelayed).subscribe(res => {
        this.nextPageTokenDelayed = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.delayedServiceOrder.items = this.delayedServiceOrder.items.concat(res['items']);
        }
      });
    } else {
      this.serviceOrder.getNextPageServiceOrderSupervisor(websafeKey, completed, this.nextPageTokenFinished).subscribe(res => {
        this.nextPageTokenFinished = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.completedServiceOrder.items = this.completedServiceOrder.items.concat(res['items']);
        }
      });
    }
  }

  moreServiceOrderTecnician(type: string) {
    const websafeKey = this.serviceStorage.getWebsafeKey();
    const inProcess = 'En Proceso';
    const delayed = 'Atrasado';
    const active = 'Activo';
    const completed = 'Terminado';
    if (type == 'inProcess') {
      this.serviceOrder.getNextPageServiceOrderTecnician(websafeKey, inProcess, this.nextPageTokenInProcess).subscribe(res => {
        this.nextPageTokenInProcess = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.inProcessServiceOrder.items = this.inProcessServiceOrder.items.concat(res['items']);
        }
      });
    } else if (type == 'active') {
      this.serviceOrder.getNextPageServiceOrderTecnician(websafeKey, active, this.nextPageTokenActive).subscribe(res => {
        this.nextPageTokenActive = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.activeServiceOrder.items = this.activeServiceOrder.items.concat(res['items']);
        }
      });
    } else if (type == 'delayed') {
      this.serviceOrder.getNextPageServiceOrderTecnician(websafeKey, delayed, this.nextPageTokenDelayed).subscribe(res => {
        this.nextPageTokenDelayed = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.delayedServiceOrder.items = this.delayedServiceOrder.items.concat(res['items']);
        }
      });
    } else {
      this.serviceOrder.getNextPageServiceOrderTecnician(websafeKey, completed, this.nextPageTokenFinished).subscribe(res => {
        this.nextPageTokenFinished = res['nextPageToken'];
        if (!res['items']) {
          alert('No hay mas elementos que mostrar');
        } else {
          this.completedServiceOrder.items = this.completedServiceOrder.items.concat(res['items']);
        }
      });
    }
  }

  downloadServiceOrder(i: number) {
    const clientId = this.completedServiceOrder.items[i].maskID;
    window.open('https://grupo-sci.appspot.com/services/report/pdf?id=' + clientId  + '&disposition=attachment');
  }

  previewServiceOrder(i: number) {
    const clientId = this.completedServiceOrder.items[i].maskID;
    window.open('https://grupo-sci.appspot.com/services/report/pdf?id=' + clientId);
  }

  deleteServiceOrder() {
    this.modalReference.close();

    if (this.delete === false) {
      const suspend = true;
      this.serviceOrder.deleteServiceOrder(this.websafeKey, suspend).subscribe(res => {

        location.reload();
      });
    } else {
      const suspend = false;
      this.serviceOrder.deleteServiceOrder(this.websafeKey, suspend).subscribe(res => {

        location.reload();
      });
    }
  }


  open(deleteModal, serviceOrder: any) {
    console.log(serviceOrder);
    this.modalReference = this.modalService.open(deleteModal, {size: 'lg'});
    this.websafeKey = serviceOrder.websafeKey;
    console.log(this.websafeKey);
  }

}
