import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import {NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { ServiceStorageService } from './../../_storage/service-storage.service';
import { UserService } from '../../_services/user.service';
import { ServiceOrderService } from './../../_services/service-order.service';

import { Cylinder } from './../../_models/cylinder-model';
import { Beams } from './../../_models/beam-model';
import { Cubes } from './../../_models/cube-model';


@Component({
  selector: 'app-edit-service-order',
  templateUrl: './edit-service-order.component.html',
  styleUrls: ['./edit-service-order.component.css']
})
export class EditServiceOrderComponent implements OnInit {

  @ViewChild('t') t;

  currentServiceOrder;
  currentJustify = 'fill';
  item = 'Cilindro';
  modalReference: any;
  closeResult: string;
  model;
  errMessage: string;
  role: string;
  age = 0;
  dateJSON;
  firstSplitDate: string;
  secondSplitDate;
  serviceKey: string;
  websafeKey: string;
  usersafeKey: string;
  userName: string;
  users: any = [];
  constructionRef: string;
  orderDate: any;
  dateTicksSO = 0;
  cylinderTab = false;
  beamTab = false;
  cubeTab = false;

  cylinderForm: FormGroup;
  cylinderArray: any = [];
  cylinder;
  cylinderRupModel;
  cylinderColModel;
  cylinderColDate;
  cylinderRupDate;
  cylinderHandlerDate;
  dateTicksCylinderCol;
  dateTicksCylinderRup;
  cylinderArea = 0;
  cylinderReskgf = 0;
  cylinderResMpa = '0';
  cylinderResPorce = 0;
  indexCylinder = 0;

  beamForm: FormGroup;
  beamArray: any = [];
  beam;
  beamRupModel;
  beamColModel;
  beamColDate;
  beamRupDate;
  beamHandlerDate;
  dateTicksBeamCol;
  dateTicksBeamRup;
  beamArea = 0;
  beamReskgf = 0;
  beamResMpa = '0';
  beamResPorce = 0;
  indexBeam = 0;
  beamCargaKn = '0';
  beamMr = 0;

  cubeForm: FormGroup;
  cubeArray: any = [];
  cube;
  cubeRupModel;
  cubeColModel;
  cubeColDate;
  cubeRupDate;
  cubeHandlerDate;
  dateTicksCubeCol = 0;
  dateTicksCubeRup = 0;
  cubeArea = 0;
  cubeCarga = '0';
  cubeResKgf = 0;
  cubeResMpa = '0';
  cubeResPorce = 0;
  indexCube = 0;

  supersafeKey: string;
  tecnicianKey: string;
  storageOrder: any;
  orderAction: string;
  component: string;
  isLastOrder = false;

  constructor(
    private serviceStorage: ServiceStorageService,
    private router: Router,
    private userService: UserService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private serviceOrder: ServiceOrderService
  ) {}

  ngOnInit() {
    this.role = this.serviceStorage.getRoleUser();
    if (this.role === 'Tecnico') {
      let splitDate, orderDate;
      this.storageOrder = JSON.parse(this.serviceStorage.getServiceOrder());
      const websafeKeyServiceOrder = this.storageOrder.websafeKey;
      this.serviceOrder.getServiceOrder(websafeKeyServiceOrder).subscribe(res => {
        this.serviceStorage.storeServiceOrder(res);
        this.currentServiceOrder = res;
        this.orderAction = res['orderAction'];
        this.usersafeKey = this.currentServiceOrder.construction.assignedTo.websafeKey;
        this.constructionRef = this.currentServiceOrder.construction.websafeKey;
        this.serviceKey = this.currentServiceOrder.websafeKey;
        if (this.currentServiceOrder.cylinderList) {
          const lengthServiceOrder = this.currentServiceOrder.cylinderList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.cylinderList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'cylinder';
          this.cylinderArray = Object.assign(this.currentServiceOrder.cylinderList);
          const mapArray = this.cylinderArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.beamTab = true;
            this.cubeTab = true;
          });
        }
        if (this.currentServiceOrder.beamList) {
          const lengthServiceOrder = this.currentServiceOrder.beamList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.beamList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'beam';
          this.beamArray = Object.assign(this.currentServiceOrder.beamList);
          const mapArray = this.beamArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.cylinderTab = true;
            this.cubeTab = true;
          });
        }
        if (this.currentServiceOrder.cubeList) {
          const lengthServiceOrder = this.currentServiceOrder.cubeList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.cubeList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }

          this.component = 'cube';
          this.cubeArray = Object.assign(this.currentServiceOrder.cubeList);
          const mapArray = this.cubeArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.cylinderTab = true;
            this.beamTab = true;
          });
        }
        splitDate = this.currentServiceOrder.orderDate.split('T')[0];
        orderDate = splitDate.split('-');
        this.model = {year: parseInt(orderDate[0], 10), month: parseInt(orderDate[1], 10),
          day: parseInt(orderDate[2], 10) };
        this.orderDate = new Date(this.model.year, this.model.month - 1, this.model.day);
        this.dateTicksSO = Math.round(this.orderDate.getTime() / 1000);
      });
    } else if (this.role === 'Admin' || this.role === 'Director') {
      let splitDate, orderDate;
      this.storageOrder = JSON.parse(this.serviceStorage.getServiceOrder());
      const websafeKeyServiceOrder = this.storageOrder.websafeKey;
      this.serviceOrder.getServiceOrder(websafeKeyServiceOrder).subscribe(res => {
        this.serviceStorage.storeServiceOrder(res);
        this.currentServiceOrder = res;
        this.orderAction = res['orderAction'];
        this.usersafeKey = this.currentServiceOrder.construction.assignedTo.websafeKey;
        this.constructionRef = this.currentServiceOrder.construction.websafeKey;
        this.serviceKey = this.currentServiceOrder.websafeKey;
        if (this.currentServiceOrder.cylinderList) {
          const lengthServiceOrder = this.currentServiceOrder.cylinderList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.cylinderList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'cylinder';
          this.cylinderArray = Object.assign(this.currentServiceOrder.cylinderList);
          const mapArray = this.cylinderArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.beamTab = true;
            this.cubeTab = true;
          });
        }
        if (this.currentServiceOrder.beamList) {
          const lengthServiceOrder = this.currentServiceOrder.beamList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.beamList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'beam';
          this.beamArray = Object.assign(this.currentServiceOrder.beamList);
          const mapArray = this.beamArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.cylinderTab = true;
            this.cubeTab = true;
          });
        }
        if (this.currentServiceOrder.cubeList) {
          const lengthServiceOrder = this.currentServiceOrder.cubeList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.cubeList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'cube';
          this.cubeArray = Object.assign(this.currentServiceOrder.cubeList);
          const mapArray = this.cubeArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.cylinderTab = true;
            this.beamTab = true;
          });
        }
        splitDate = this.currentServiceOrder.orderDate.split('T')[0];
        orderDate = splitDate.split('-');
        this.model = {year: parseInt(orderDate[0], 10), month: parseInt(orderDate[1], 10),
          day: parseInt(orderDate[2], 10) };
        this.orderDate = new Date(this.model.year, this.model.month - 1, this.model.day);
        this.dateTicksSO = Math.round(this.orderDate.getTime() / 1000);
      });
      const role = 'Tecnico';
      this.userService.getAllUsersByRole(role).subscribe(response => {
        this.users = response;
      });
    } else {
      let splitDate, orderDate;
      this.storageOrder = JSON.parse(this.serviceStorage.getServiceOrder());
      const websafeKeyServiceOrder = this.storageOrder.websafeKey;
      this.serviceOrder.getServiceOrder(websafeKeyServiceOrder).subscribe(res => {
        this.serviceStorage.storeServiceOrder(res);
        this.currentServiceOrder = res;
        this.orderAction = res['orderAction'];
        this.usersafeKey = this.currentServiceOrder.construction.assignedTo.websafeKey;
        this.constructionRef = this.currentServiceOrder.construction.websafeKey;
        this.serviceKey = this.currentServiceOrder.websafeKey;
        if (this.currentServiceOrder.cylinderList) {
          const lengthServiceOrder = this.currentServiceOrder.cylinderList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.cylinderList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'cylinder';
          this.cylinderArray = Object.assign(this.currentServiceOrder.cylinderList);
          const mapArray = this.cylinderArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.beamTab = true;
            this.cubeTab = true;
          });
        }
        if (this.currentServiceOrder.beamList) {
          const lengthServiceOrder = this.currentServiceOrder.beamList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.beamList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'beam';
          this.beamArray = Object.assign(this.currentServiceOrder.beamList);
          const mapArray = this.beamArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
            this.cylinderTab = true;
            this.cubeTab = true;
          });
        }
        if (this.currentServiceOrder.cubeList) {
          const lengthServiceOrder = this.currentServiceOrder.cubeList.length;
          const finalRupDate = new Date(parseInt( this.currentServiceOrder.cubeList[lengthServiceOrder - 1].ruptura, 10) * 1000);
          const serviceOrderDate = this.currentServiceOrder.orderDate;
          if (finalRupDate.toISOString() === serviceOrderDate) {
            this.isLastOrder = true;
          }
          this.component = 'cube';
          this.cubeArray = Object.assign(this.currentServiceOrder.cubeList);
          const mapArray = this.cubeArray.map( obj => {
            const timeStampCol = (parseInt(obj.colado, 10) * 1000);
            const timeStampRup = (parseInt(obj.ruptura, 10) * 1000);
            const dateCol = new Date(timeStampCol);
            const dateRup = new Date(timeStampRup);
            const dateStringCol = dateCol.toDateString();
            const dateStringRup = dateRup.toDateString();
            obj.colado = dateStringCol;
            obj.ruptura = dateStringRup;
          });
        }
        splitDate = this.currentServiceOrder.orderDate.split('T')[0];
        orderDate = splitDate.split('-');
        this.model = {year: parseInt(orderDate[0], 10), month: parseInt(orderDate[1], 10),
          day: parseInt(orderDate[2], 10) };
        this.orderDate = new Date(this.model.year, this.model.month - 1, this.model.day);
        this.dateTicksSO = Math.round(this.orderDate.getTime() / 1000);
      });
      const supervisorsafeKey = this.serviceStorage.getWebsafeKey();
      this.userService.getAllTecnicianSuper(supervisorsafeKey).subscribe(data => {
        this.users = data;
      });
    }
  }

  userInfo(name: string, usersafeKey: string) {
    this.errMessage = null;
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }

  // Just to changue the display name in the add button
  tabName($event: NgbTabChangeEvent) {
    if ($event.nextId === 'cylinders') {
        this.item = 'Cilindro';
    } else if ($event.nextId === 'beams') {
        this.item = 'Viga';
    } else if ($event.nextId === 'cubes') {
        this.item = 'Cubo';
    }
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onChangesEditedCylinder(): void {
    this.cylinderForm.valueChanges.subscribe(val => {
      this.cylinder.area = (Math.pow((val.diametro / 2), 2) * (3.1416));
      this.cylinder.resistenciaKgf = Math.round(val.cargaKgf / this.cylinder.area);
      this.cylinder.resistenciaMpa = Number( this.cylinder.resistenciaKgf / 10.19716).toFixed(1);
      this.cylinder.resistenciaPorce = Math.round((this.cylinder.resistenciaKgf / this.cylinder.resistenciaProy) * 100);
      this.cylinderArea = this.cylinder.area;
      this.cylinderReskgf = this.cylinder.resistenciaKgf;
      this.cylinderResMpa = this.cylinder.resistenciaMpa;
      this.cylinderResPorce = this.cylinder.resistenciaPorce;
    });
  }

  onChangesEditedBeam(): void {
    this.beamForm.valueChanges.subscribe(val => {
      this.beam.cargaKn = Number(val.cargaKgf / 101.9).toFixed(1);
      this.beam.mr =  Math.round((val.cargaKgf * val.distanciaEntreApoyos) / (val.ancho * (val.ancho * val.ancho)));
      this.beam.resistenciaMpa = Number(this.beam.mr / 10.19716).toFixed(1);
      this.beam.resistenciaPorce = Math.round((this.beam.mr / this.beam.mrProy) * 100);
      this.beamCargaKn = this.beam.cargaKn;
      this.beamMr = this.beam.mr;
      this.beamResMpa = this.beam.resistenciaMpa;
      this.beamResPorce = this.beam.resistenciaPorce;
    });
  }

  onChangesEditedCube(): void {
    this.cubeForm.valueChanges.subscribe(val => {
      this.cube.area = (val.largo * val.ancho);
      this.cube.cargaKn = Number(val.cargaKgf / 101.9).toFixed(1);
      this.cube.resistenciaKgf = (val.cargaKgf / this.cubeArea);
      this.cube.resistenciaMpa = Number(this.cubeResKgf / 10.19716).toFixed(1);
      this.cube.resistenciaPorce = ((this.cube.resistenciaKgf / this.cube.resistenciaProy) * 100);
      this.cubeArea = this.cube.area;
      this.cubeCarga = this.cube.carga;
      this.cubeResKgf = this.cube.resistenciaKgf;
      this.cubeResMpa = this.cube.resistenciaMpa;
      this.cubeResPorce = this.cube.resistenciaPorce;
    });
  }

  createCylinderForm() {
    this.cylinderForm = this.fb.group({
      rev: new FormControl('', Validators.required),
      resistenciaProy: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createEditedCylinderForm() {
    this.cylinderForm = this.fb.group ({
      edad: new FormControl({value: '0', disabled: true}),
      rev: new FormControl({value: '0', disabled: true}, Validators.required),
      diametro: new FormControl('0', Validators.required),
      altura: new FormControl('0', Validators.required),
      area: new FormControl({value: '0', disabled: true}),
      cargaKn: new FormControl('0', Validators.required),
      cargaKgf: new FormControl('0', Validators.required),
      resistenciaKgf: new FormControl({value: '0', disabled: true}),
      resistenciaMpa: new FormControl({value: '0', disabled: true}),
      resistenciaProy: new FormControl({value: '0', disabled: true}),
      resistenciaPorce: new FormControl({value: '0', disabled: true}),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createBeamForm() {
    this.beamForm = this.fb.group({
      rev: new FormControl('', Validators.required),
      mrProy: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createEditedBeamForm() {
    this.beamForm = this.fb.group({
      edad: new FormControl({value: '0', disabled: true}),
      rev: new FormControl({value: '0', disabled: true}),
      ancho: new FormControl('0', Validators.required),
      peralte: new FormControl('0', Validators.required),
      distanciaEntreApoyos: new FormControl('0', Validators.required),
      cargaKn: new FormControl({value: '0', disabled: true}),
      cargaKgf: new FormControl('0', Validators.required),
      mr: new FormControl({value: '0', disabled: true}),
      resistenciaMpa: new FormControl({value: '0', disabled: true}),
      mrProy: new FormControl({value: '0', disabled: true}),
      resistenciaPorce: new FormControl({value: '0', disabled: true}),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createCubeForm() {
    this.cubeForm = this.fb.group({
      rev: new FormControl('0'),
      resistenciaProy: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createEditedCubeForm() {
    this.cubeForm = this.fb.group({
      edad: new FormControl({value: '0', disabled: true}),
      rev: new FormControl('0', Validators.required),
      largo: new FormControl('0', Validators.required),
      ancho: new FormControl('0', Validators.required),
      area: new FormControl({value: '0', disabled: true}),
      cargaKn: new FormControl({value: '0', disabled: true}),
      cargaKgf: new FormControl('0', Validators.required),
      resistenciaKgf: new FormControl({value: '0', disabled: true}),
      resistenciaMpa: new FormControl({value: '0', disabled: true}),
      resistenciaProy: new FormControl({value: '0', disabled: true}),
      resistenciaPorce: new FormControl({value: '0', disabled: true}),
      ubicacion: new FormControl('', Validators.required)
    });
  }


  addCylinder(cylinderModal) {
    this.modalReference = this.modalService.open(cylinderModal, {size: 'lg', backdrop: false});
    this.createCylinderForm();
    this.cylinder = new Cylinder();
  }

  addBeam(beamModal, item) {
    this.modalReference = this.modalService.open(beamModal, {size: 'lg', backdrop: false });
    this.createBeamForm();
    this.beam = new Beams();
  }

  addCube(cubeModal, item) {
    this.modalReference = this.modalService.open(cubeModal, {size: 'lg', backdrop: false });
    this.createCubeForm();
    this.cube = new Cubes();
  }

  saveCylinder() {
      const cylinderColDate = new Date(this.cylinderColModel.year, this.cylinderColModel.month - 1, this.cylinderColModel.day);
      const cylinderRupDate = new Date(this.cylinderRupModel.year, this.cylinderRupModel.month - 1, this.cylinderRupModel.day);
      const timeStampServOrder = Math.round(cylinderColDate.getTime() / 1000);
      const timeStampRup = Math.round(cylinderRupDate.getTime() / 1000);
      const cylinder = Object.assign(this.cylinder, this.cylinderForm.value);
      const timeDiff = Math.abs(cylinderColDate.getTime() - cylinderRupDate.getTime());
      const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      cylinder.colado = timeStampServOrder;
      cylinder.ruptura = timeStampRup;
      cylinder.edad = diffDays;
      this.serviceOrder.postNewCylinder(this.serviceKey, cylinder).subscribe(res => {
        this.modalReference.close();
        location.reload();
      });
  }

  saveBeam() {
    const beamColDate = new Date(this.beamColModel.year, this.beamColModel.month - 1, this.beamColModel.day);
    const beamRupDate = new Date(this.beamRupModel.year, this.beamRupModel.month - 1, this.beamRupModel.day);
    const timeStampServOrder = Math.round(beamColDate.getTime() / 1000);
    const timeStampRup = Math.round(beamRupDate.getTime() / 1000);
    const beam = Object.assign(this.beam, this.beamForm.value);
    const timeDiff = Math.abs(beamColDate.getTime() - beamRupDate.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    beam.colado = timeStampServOrder;
    beam.ruptura = timeStampRup;
    beam.edad = diffDays;
    this.serviceOrder.postNewBeam(this.serviceKey, beam).subscribe(res => {
      this.modalReference.close();
      location.reload();
    });
  }

  saveCube() {
    const cubeColDate = new Date(this.cubeColModel.year, this.cubeColModel.month - 1, this.cubeColModel.day);
    const cubeRupDate = new Date(this.cubeRupModel.year, this.cubeRupModel.month - 1, this.cubeRupModel.day);
    const timeStampServOrder = Math.round(cubeColDate.getTime() / 1000);
    const timeStampRup = Math.round(cubeRupDate.getTime() / 1000);
    const cube = Object.assign(this.cube, this.cubeForm.value);
    const timeDiff = Math.abs(cubeColDate.getTime() - cubeRupDate.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    cube.colado = timeStampServOrder;
    cube.ruptura = timeStampRup;
    cube.edad = diffDays;
    this.serviceOrder.postNewCube(this.serviceKey, cube).subscribe(res => {
      this.modalReference.close();
      location.reload();
    });
  }

  editCylinder(editCylinderContent, i: number) {
    const webKey = JSON.parse(this.serviceStorage.getServiceOrder());
    this.websafeKey = webKey.cylinderList[i].websafeKey;
    this.indexCylinder = i;
    this.createEditedCylinderForm();
    this.onChangesEditedCylinder();
    this.modalReference = this.modalService.open(editCylinderContent, {size: 'lg', backdrop: false});
    this.cylinder = this.storageOrder.cylinderList[i];
    this.cylinderForm.patchValue({
      colado: this.cylinder.colado,
      ruptura: this.cylinder.ruptura,
      edad: this.cylinder.edad,
      rev: this.cylinder.rev,
      diametro: this.cylinder.diametro,
      altura: this.cylinder.altura,
      area: this.cylinder.area,
      cargaKn: this.cylinder.cargaKn,
      cargaKgf: this.cylinder.cargaKgf,
      resistenciaKgf: this.cylinder.resistenciaKgf,
      resistenciaMpa: this.cylinder.resistenciaMpa,
      resistenciaProy: this.cylinder.resistenciaProy,
      resistenciaPorce: this.cylinder.resistenciaPorce,
      ubicacion: this.cylinder.ubicacion
    });

  }

  editBeam(editBeamContent, i: number) {
    const webKey = JSON.parse(this.serviceStorage.getServiceOrder());
    this.websafeKey = webKey.beamList[i].websafeKey;
    this.indexBeam = i;
    this.createEditedBeamForm();
    this.onChangesEditedBeam();
    this.modalReference = this.modalService.open(editBeamContent, {size: 'lg', backdrop: false});
    this.beam = this.storageOrder.beamList[i];
    this.beamForm.patchValue({
      colado: this.beam.colado,
      ruptura: this.beam.ruptura,
      edad: this.beam.edad,
      rev: this.beam.rev,
      ancho: this.beam.ancho,
      peralte: this.beam.peralte,
      distanciaEntreApoyos: this.beam.distanciaEntreApoyos,
      cargaKn: this.beam.cargaKn,
      cargaKgf: this.beam.cargaKgf,
      mr: this.beam.mr,
      resistenciaMpa: this.beam.resistenciaMpa,
      mrProy: this.beam.mrProy,
      resistenciaPorce: this.beam.resistenciaPorce,
      ubicacion: this.beam.ubicacion
    });
  }

  editCube(editCubeContent, i: number) {
    const webKey = JSON.parse(this.serviceStorage.getServiceOrder());
    this.websafeKey = webKey.cubeList[i].websafeKey;
    this.indexCube = i;
    this.createEditedCubeForm();
    this.onChangesEditedCube();
    this.modalReference = this.modalService.open(editCubeContent, {size: 'lg', backdrop: false});
    this.cube = this.storageOrder.cubeList[i];
    this.cubeForm.patchValue({
      colado: this.cube.colado,
      ruptura: this.cube.ruptura,
      edad: this.cube.edad,
      rev: this.cube.rev,
      largo: this.cube.largo,
      ancho: this.cube.ancho,
      area: this.cube.area,
      cargaKn: this.cube.cargaKn,
      cargaKgf: this.cube.cargaKgf,
      resistenciaKgf: this.cube.resistenciaKgf,
      resistenciaMpa: this.cube.resistenciaMpa,
      resistenciaProy: this.cube.resistenciaProy,
      resistenciaPorce: this.cube.resistenciaPorce,
      ubicacion: this.cube.ubicacion
    });
  }

  saveEditedCylinder() {
    if (confirm('¿Los datos son correctos? Una vez guardados los cambios la orden de servicio se actualizara.') === true) {
      this.cylinderForm.patchValue({
        area: this.cylinderArea,
        resistenciaKgf: this.cylinderReskgf,
        resistenciaMpa: this.cylinderResMpa,
        resistenciaPorce: this.cylinderResPorce,
      });
      const cylinder = JSON.parse(this.serviceStorage.getServiceOrder());
      const rawForm = Object.assign(this.cylinderForm.getRawValue(), this.cylinderForm.value);
      rawForm.colado = this.cylinder.colado;
      rawForm.ruptura = this.cylinder.ruptura;
      if (this.isLastOrder === false) {
        const nextRup = cylinder.cylinderList[this.indexCylinder + 1].ruptura;
        this.serviceOrder.editCylinder(this.websafeKey, rawForm).subscribe(res => {
          this.serviceOrder.updateRuptura(this.serviceKey, nextRup).subscribe(response => {
             this.modalReference.close();
             location.reload();
          });
       });
      } else {
        this.serviceOrder.editCylinder(this.websafeKey, rawForm).subscribe(res => {
            this.modalReference.close();
            location.reload();
        });
      }
   } else {}
  }

  saveEditedBeam() {
    if (confirm('¿Los datos son correctos? Una vez guardados los cambios la orden de servicio se actualizara.') === true) {
      this.beamForm.patchValue({
        cargaKn:  this.beamCargaKn,
        mr: this.beamMr,
        resistenciaMpa: this.beamResMpa,
        resistenciaPorce: this.beamResPorce,
      });
      const beam = JSON.parse(this.serviceStorage.getServiceOrder());
      const rawForm = Object.assign(this.beamForm.getRawValue(), this.beamForm.value);
      rawForm.colado = this.beam.colado;
      rawForm.ruptura = this.beam.ruptura;
       if (this.isLastOrder === false) {
        const nextRup = beam.beamList[this.indexBeam + 1].ruptura;
        this.serviceOrder.editBeam(this.websafeKey, rawForm).subscribe(res => {
          this.serviceOrder.updateRuptura(this.serviceKey, nextRup).subscribe(response => {
            this.modalReference.close();
            location.reload();
          });
        });
       } else {
         this.serviceOrder.editBeam(this.websafeKey, rawForm).subscribe(res => {
          this.modalReference.close();
          location.reload();
         });
       }
   } else {}
  }

  saveEditedCube() {
    if (confirm('¿Los datos son correctos? Una vez guardados los cambios la orden de servicio se actualizara.') === true) {
      this.cubeForm.patchValue({
        area: this.cubeArea,
        cargaKn: this.cubeCarga,
        resistenciaKgf: this.cubeResKgf,
        resistenciaMpa: this.cubeResMpa,
        resistenciaPorce: this.cubeResPorce,
      });
      const cube = JSON.parse(this.serviceStorage.getServiceOrder());
      const rawForm = Object.assign(this.cubeForm.getRawValue(), this.cubeForm.value);
      rawForm.colado = this.cube.colado;
      rawForm.ruptura = this.cube.ruptura;
      if (this.isLastOrder === false) {
        const nextRup = cube.cubeList[this.indexCube + 1].ruptura;
        this.serviceOrder.editCube(this.websafeKey, rawForm).subscribe(res => {
          this.serviceOrder.updateRuptura(this.serviceKey, nextRup).subscribe(response => {
            this.modalReference.close();
            location.reload();
          });
        });
      } else {
        this.serviceOrder.editCube(this.websafeKey, rawForm).subscribe(res => {
          this.modalReference.close();
          location.reload();
        });
      }
   } else {}
  }

  deleteCylinder(i: number) {
    if (confirm('¿Seguro que desea borrar el cilindro?') === true) {
      this.serviceOrder.deleteCylinder(this.cylinderArray[i].websafeKey, this.serviceKey).subscribe(res => {
        this.cylinderArray.splice(i, 1);
      });
   } else {}
  }

  deleteBeam(i: number) {
    if (confirm('¿Seguro que desea borrar el viga?') === true) {
      this.serviceOrder.deleteBeam(this.beamArray[i].websafeKey, this.serviceKey).subscribe(res => {
        this.beamArray.splice(i, 1);
      });
   } else {}
  }

  deleteCube(i: number) {
    if (confirm('¿Seguro que desea borrar el cubo?') === true) {
      this.serviceOrder.deleteCube(this.cubeArray[i].websafeKey, this.serviceKey).subscribe(res => {
        this.cubeArray.splice(i, 1);
      });
   } else {}
  }

  moreCylinder(seeMoreCylinder, index: number) {
    this.modalReference = this.modalService.open(seeMoreCylinder);
    this.cylinder = this.currentServiceOrder.cylinderList[index];
  }

  moreBeam(seeMoreBeam, index: number) {
    this.modalReference = this.modalService.open(seeMoreBeam);
    this.beam = this.currentServiceOrder.beamList[index];
  }

  moreCube(seeMoreCube, index: number) {
    this.modalReference = this.modalService.open(seeMoreCube);
    this.cube = this.currentServiceOrder.cubeList[index];
  }

  save() {
    if (this.role === 'Admin' || this.role === 'Director') {
      this.router.navigate(['/home/service-order']);
    } else if (this.role === 'Tecnico') {
      this.router.navigate(['/home-tecnician/service-order']);
    } else if (this.role === 'Supervisor') {
      this.router.navigate(['/home-supervisor/service-order']);
    }
  }

  cancelOrder() {
    if (confirm('¿Seguro deseas salir de la orden?') === true) {
      if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/service-order']);
      } else if (this.role === 'Tecnico') {
        this.router.navigate(['/home-tecnician/service-order']);
      } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/service-order']);
      }
  } else {}
  }

  finishServiceOrder() {
    this.serviceOrder.finishServiceOrder(this.serviceKey).subscribe(res => {
      if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/service-order']);
      } else if (this.role === 'Tecnico') {
        this.router.navigate(['/home-tecnician/service-order']);
      } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/service-order']);
      }
    });
  }

  finishSample() {
    if (this.component == 'cylinder') {
      const orderDate = this.currentServiceOrder.orderDate;
      const coladoServiceOrder = new Date(parseInt(this.storageOrder.cylinderList[0].colado, 10) * 1000);
      if (coladoServiceOrder.toISOString() === orderDate) {
        this.serviceOrder.updateRuptura(this.serviceKey, this.storageOrder.cylinderList[0].ruptura).subscribe(res => {
          if (this.role === 'Admin' || this.role === 'Director') {
            this.router.navigate(['/home/service-order']);
          } else if (this.role === 'Tecnico') {
            this.router.navigate(['/home-tecnician/service-order']);
          } else if (this.role === 'Supervisor') {
            this.router.navigate(['/home-supervisor/service-order']);
          }
        });
      }
    } else if (this.component == 'beam') {
      const orderDate = this.currentServiceOrder.orderDate;
      const coladoServiceOrder = new Date(parseInt(this.storageOrder.beamList[0].colado, 10) * 1000);
      if (coladoServiceOrder.toISOString() === orderDate) {
        this.serviceOrder.updateRuptura(this.serviceKey, this.storageOrder.beamList[0].ruptura).subscribe(res => {
          if (this.role === 'Admin' || this.role === 'Director') {
            this.router.navigate(['/home/service-order']);
          } else if (this.role === 'Tecnico') {
            this.router.navigate(['/home-tecnician/service-order']);
          } else if (this.role === 'Supervisor') {
            this.router.navigate(['/home-supervisor/service-order']);
          }
        });
      }
    } else {
      const orderDate = this.currentServiceOrder.orderDate;
      const coladoServiceOrder = new Date(parseInt(this.storageOrder.cubeList[0].colado, 10) * 1000);
      if (coladoServiceOrder.toISOString() === orderDate) {
        this.serviceOrder.updateRuptura(this.serviceKey, this.storageOrder.cubeList[0].ruptura).subscribe(res => {
          if (this.role === 'Admin' || this.role === 'Director') {
            this.router.navigate(['/home/service-order']);
          } else if (this.role === 'Tecnico') {
            this.router.navigate(['/home-tecnician/service-order']);
          } else if (this.role === 'Supervisor') {
            this.router.navigate(['/home-supervisor/service-order']);
          }
        });
      }
    }
  }


}
