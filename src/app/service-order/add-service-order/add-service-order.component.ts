import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateAdapter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { ServiceOrderService } from '../../_services/service-order.service';
import { StructureService } from './../../_services/structures.service';
import { UserService } from '../../_services/user.service';
import { ServiceStorageService } from '../../_storage/service-storage.service';

import { Cylinder } from './../../_models/cylinder-model';
import { Beams } from './../../_models/beam-model';
import { Cubes } from './../../_models/cube-model';

@Component({
  selector: 'app-add-service-order',
  templateUrl: './add-service-order.component.html',
  styleUrls: ['./add-service-order.component.css'],
  animations: []
})
export class AddServiceOrderComponent implements OnInit {



  // Beam code and variables
  indexBeam = 0;
  beamRupModel;
  beamColDate;
  beamHandlerDate;
  dateTicksBeamCol;
  dateTicksBeamRup;
  beamCargaKn = '0';
  beamMr = 0;
  beamResMpa = '0';
  beamResPorce = 0;
  beamCarKn = 0;
  // Cube code and variables
  indexCube = 0;
  cubeRupModel;
  cubeColDate;
  cubeHandlerDate;
  dateTicksCubeCol = 0;
  dateTicksCubeRup = 0;
  cubeArea = 0;
  cubeCarga = 0;
  cubeResKgf = 0;
  cubeResMpa = '0';
  cubeResPorce = 0;


  timeStampServOrder: number;
  role: string;
  structures: any = [];
  users: any = [];
  structureName = 'Obras';
  websafeKey: any;
  usersafeKey: string;
  userName = 'Seleccionar';
  modalReference: any;
  errMessage: string;
  currentJustify = 'fill';
  item = 'Cilindro';
  editedRupModel;



  cylinderTab = false;
  beamTab = false;
  cubeTab = false;


  cylinderForm: FormGroup;
  cylinderColModel;
  cylinder;
  cylinderRefsList: any = [];
  cylinderTable: any = [];
  editedCylinderIndex = 0;
  isFirstCylinderAdd = true;

  beamForm: FormGroup;
  beamColModel;
  beam;
  beamRefsList: any = [];
  beamTable: any = [];
  editedBeamIndex = 0;
  isFirstBeamAdd = true;

  cubeForm: FormGroup;
  cubeColModel;
  cube;
  cubeRefsList: any = [];
  cubeTable: any = [];
  editedCubeIndex = 0;
  isFirstCubeAdd = true;


  constructor(
    private router: Router,
    private structureService: StructureService,
    private serviceOrderService: ServiceOrderService,
    private userService: UserService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private serviceStorage: ServiceStorageService
  ) {}

  ngOnInit() {
    this.role = this.serviceStorage.getRoleUser();
    if (this.role === 'Admin' || this.role === 'Director') {
      const role = 'Tecnico';
      this.structureService.getAllStructures().subscribe(data => {
        this.structures = data;
      });
      this.userService.getAllUsersByRole(role).subscribe(response => {
        this.users = response;
      });
    } else if (this.role === 'Supervisor') {
      const supervisorsafeKey = this.serviceStorage.getWebsafeKey();
      this.structureService.getAllStructures().subscribe(data => {
        this.structures = data;
      });
      this.userService.getAllTecnicianSuper(supervisorsafeKey).subscribe(data => {
          this.users = data;
        });
    } else {
      const user = JSON.parse(this.serviceStorage.getUserSession());
      this.usersafeKey = user.websafeKey;
      this.structureService.getAllStructures().subscribe(data => {
        this.structures = data;
      });
    }
  }

  // Just to changue the display name in the add button
  tabName($event: NgbTabChangeEvent) {
    if ($event.nextId === 'cylinders') {
      this.item = 'Cilindro';
    } else if ($event.nextId === 'beams') {
      this.item = 'Viga';
    } else if ($event.nextId === 'cubes') {
      this.item = 'Cubo';
    }
  }

  // This method is to obtain the websafekey and the of the structure for the post service
  structureInfo(structureName: string, websafeKey: string) {
    this.structureName = structureName;
    this.websafeKey = websafeKey;
  }

  userInfo(name: string, usersafeKey: string) {
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }
  // Create de forms of all the elements
  createCylinderForm() {
    this.cylinderForm = this.fb.group({
      rev: new FormControl('', Validators.required),
      resistenciaProy: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createBeamForm() {
    this.beamForm = this.fb.group({
      rev: new FormControl('', Validators.required),
      mrProy: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required)
    });
  }

  createCubeForm() {
    this.cubeForm = this.fb.group({
      rev: new FormControl('0'),
      resistenciaProy: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required)
    });
  }
  // method to open the respective modal
  addCylinder(cylinderModal) {
      this.modalReference = this.modalService.open(cylinderModal, { size: 'lg', backdrop: false });
      this.createCylinderForm();
      this.cylinder = new Cylinder();
  }
  addBeam(beamModal) {
      this.modalReference = this.modalService.open(beamModal, {size: 'lg', backdrop: false });
      this.createBeamForm();
      this.beam = new Beams();
  }
  addCube(cubeModal) {
      this.modalReference = this.modalService.open(cubeModal, {size: 'lg', backdrop: false });
      this.createCubeForm();
      this.cube = new Cubes();
  }
  // method to create the rupture in the cylinder
  ruptureDate(day: number, colModel: any) {
    const rupDate = new Date(colModel.year, colModel.month - 1, colModel.day );
    rupDate.setDate(colModel.day + day);
    const rupTimeStamp = Math.round(rupDate.getTime() / 1000);
    return rupTimeStamp;
  }
  ruptureTableDate(day: number, colModel: any) {
    const rupDate = new Date( colModel.year, colModel.month - 1, colModel.day );
    rupDate.setDate(colModel.day + day);
    return rupDate.toDateString();
  }
  // methods to save the model
  saveCylinder() {
    let starterDay = 7;
    if (this.isFirstCylinderAdd === true) {
      const cylinderColDate = new Date(this.cylinderColModel.year, this.cylinderColModel.month - 1, this.cylinderColModel.day);
      this.timeStampServOrder = Math.round(cylinderColDate.getTime() / 1000);
      for (let i = 0; i < 4; i++) {
        const cylinder = Object.assign({}, this.cylinder, this.cylinderForm.value);
        cylinder.colado = this.timeStampServOrder;
        cylinder.ruptura = this.ruptureDate(starterDay, this.cylinderColModel);
        cylinder.edad = starterDay;
        const cylinderTable = Object.assign({}, cylinder);
        cylinderTable.colado = cylinderColDate.toDateString();
        cylinderTable.ruptura = this.ruptureTableDate(starterDay, this.cylinderColModel);
        cylinderTable.edad = starterDay;
        this.cylinderRefsList.push(cylinder);
        this.cylinderTable.push(cylinderTable);
        starterDay = starterDay * 2;
      }
      this.serviceStorage.setCylinderArray(this.cylinderRefsList);
      this.isFirstCylinderAdd = false;
      this.beamTab = true;
      this.cubeTab = true;
      this.modalReference.close();
    } else {
      const cylinderColDate = new Date(this.cylinderColModel.year, this.cylinderColModel.month - 1, this.cylinderColModel.day);
      const timeStampServOrder = Math.round(cylinderColDate.getTime() / 1000);
      const cylinder = Object.assign(this.cylinder, this.cylinderForm.value);
      cylinder.colado = timeStampServOrder;
      const cylinderTable = Object.assign({}, cylinder);
      cylinderTable.colado = cylinderColDate.toDateString();
      this.cylinderRefsList.push(cylinder);
      this.cylinderTable.push(cylinderTable);
      this.serviceStorage.setCylinderArray(this.cylinderRefsList);
      this.modalReference.close();
    }
  }
  saveBeam() {
    let starterDay = 7;
    if (this.isFirstBeamAdd === true) {
      const beamColDate = new Date(this.beamColModel.year, this.beamColModel.month - 1, this.beamColModel.day);
      this.timeStampServOrder = Math.round(beamColDate.getTime() / 1000);
      for (let i = 0; i < 4; i++) {
        const beam = Object.assign({}, this.beam, this.beamForm.value);
        beam.colado = this.timeStampServOrder;
        beam.ruptura = this.ruptureDate(starterDay, this.beamColModel);
        beam.edad = starterDay;
        const beamTable = Object.assign({}, beam);
        beamTable.colado = beamColDate.toDateString();
        beamTable.ruptura = this.ruptureTableDate(starterDay, this.beamColModel);
        beamTable.edad = starterDay;
        this.beamRefsList.push(beam);
        this.beamTable.push(beamTable);
        starterDay = starterDay * 2;
      }
      this.serviceStorage.setBeamArray(this.beamRefsList);
      this.isFirstBeamAdd = false;
      this.cylinderTab = true;
      this.cubeTab = true;
      this.modalReference.close();
    } else {
      const beamColDate = new Date(this.beamColModel.year, this.beamColModel.month - 1, this.beamColModel.day);
      const timeStampServOrder = Math.round(beamColDate.getTime() / 1000);
      const beam = Object.assign(this.beam, this.beamForm.value);
      beam.colado = timeStampServOrder;
      const beamTable = Object.assign({}, beam);
      beamTable.colado = beamColDate.toDateString();
      this.beamRefsList.push(beam);
      this.beamTable.push(beamTable);
      this.serviceStorage.setBeamArray(this.beamRefsList);
      this.modalReference.close();
    }
  }
  saveCube() {
    let starterDay = 7;
    if (this.isFirstCubeAdd === true) {
      const cubeColDate = new Date(this.cubeColModel.year, this.cubeColModel.month - 1, this.cubeColModel.day);
      this.timeStampServOrder = Math.round(cubeColDate.getTime() / 1000);
      for (let i = 0; i < 4; i++) {
        const cube = Object.assign({}, this.cube, this.cubeForm.value);
        cube.colado = this.timeStampServOrder;
        cube.ruptura = this.ruptureDate(starterDay, this.cubeColModel);
        cube.edad = starterDay;
        const cubeTable = Object.assign({}, cube);
        cubeTable.colado = cubeColDate.toDateString();
        cubeTable.ruptura = this.ruptureTableDate(starterDay, this.cubeColModel);
        cubeTable.edad = starterDay;
        this.cubeRefsList.push(cube);
        this.cubeTable.push(cubeTable);
        starterDay = starterDay * 2;
      }
      this.serviceStorage.setCubeArray(this.cubeRefsList);
      this.isFirstCubeAdd = false;
      this.cylinderTab = true;
      this.beamTab = true;
      this.modalReference.close();
    } else {
      const cubeColDate = new Date(this.cubeColModel.year, this.cubeColModel.month - 1, this.cubeColModel.day);
      const timeStampServOrder = Math.round(cubeColDate.getTime() / 1000);
      const cube = Object.assign(this.cube, this.cubeForm.value);
      cube.colado = timeStampServOrder;
      const cubeTable = Object.assign({}, cube);
      cubeTable.colado = cubeColDate.toDateString();
      this.cubeRefsList.push(cube);
      this.cubeTable.push(cubeTable);
      this.serviceStorage.setCubeArray(this.cubeRefsList);
      this.modalReference.close();
    }
  }
  // Method to see the modal
  moreCylinder(seeMoreCylinder, index: number) {
    this.modalReference = this.modalService.open(seeMoreCylinder);
    const cylinderMore = JSON.parse(this.serviceStorage.getCylinderArray());
    this.cylinder = cylinderMore[index];
  }
  moreBeam(seeMoreBeam, index: number) {
    this.modalReference = this.modalService.open(seeMoreBeam);
    const beamMore = JSON.parse(this.serviceStorage.getBeamArray());
    this.beam = beamMore[index];
  }
  moreCube(seeMoreCube, index: number) {
    this.modalReference = this.modalService.open(seeMoreCube);
    const cubeMore = JSON.parse(this.serviceStorage.getCubeArray());
    this.cube = cubeMore[index];
  }
  // methods to edit the info
  editCylinder(editCylinderContent, index: number) {
    this.editedCylinderIndex = index;
    const cylinderMore = JSON.parse(this.serviceStorage.getCylinderArray());
    this.cylinder = cylinderMore[index];
    if (this.cylinder.ruptura !== 0) {
      const rupDate = new Date(this.cylinder.ruptura * 1000);
      const month = rupDate.getMonth() + 1;
      const day = rupDate.getDate();
      const year = rupDate.getFullYear();
      this.editedRupModel = {year: year, month: month, day: day};
      this.modalReference = this.modalService.open(editCylinderContent, { size: 'lg', backdrop: false });
    } else {
      this.modalReference = this.modalService.open(editCylinderContent, { size: 'lg', backdrop: false });
    }
  }
  editBeam(editBeamContent, index: number) {
    this.editedBeamIndex = index;
    const beamMore = JSON.parse(this.serviceStorage.getBeamArray());
    this.beam = beamMore[index];
    if (this.beam.ruptura !== 0) {
      const rupDate = new Date(this.beam.ruptura * 1000);
      const month = rupDate.getMonth() + 1;
      const day = rupDate.getDate();
      const year = rupDate.getFullYear();
      this.editedRupModel = {year: year, month: month, day: day};
      this.modalReference = this.modalService.open(editBeamContent, { size: 'lg', backdrop: false });
    } else {
      this.modalReference = this.modalService.open(editBeamContent, { size: 'lg', backdrop: false });
    }
  }
  editCube(editCubeContent, index: number) {
    this.editedCubeIndex = index;
    const cubeMore = JSON.parse(this.serviceStorage.getCubeArray());
    this.cube = cubeMore[index];
    if (this.cube.ruptura !== 0) {
      const rupDate = new Date(this.cube.ruptura * 1000);
      const month = rupDate.getMonth() + 1;
      const day = rupDate.getDate();
      const year = rupDate.getFullYear();
      this.editedRupModel = {year: year, month: month, day: day};
      this.modalReference = this.modalService.open(editCubeContent, { size: 'lg', backdrop: false });
    } else {
      this.modalReference = this.modalService.open(editCubeContent, { size: 'lg', backdrop: false });
    }
  }
  // method to save the edited element
  saveEditedCylinder() {
    const cylinder = JSON.parse(this.serviceStorage.getCylinderArray())[this.editedCylinderIndex];
    const cylinderTable = this.cylinderTable[this.editedCylinderIndex];
    cylinder.rev = this.cylinderForm.get('rev').value;
    cylinder.resistenciaProy = this.cylinderForm.get('resistenciaProy').value;
    cylinder.ubicacion = this.cylinderForm.get('ubicacion').value;
    cylinderTable.rev = this.cylinderForm.get('rev').value;
    cylinderTable.resistenciaProy = this.cylinderForm.get('resistenciaProy').value;
    cylinderTable.ubicacion = this.cylinderForm.get('ubicacion').value;
    const cylinderRupDate = new Date(this.editedRupModel.year, this.editedRupModel.month - 1, this.editedRupModel.day);
    const cylinderRupTimeStamp = Math.round(cylinderRupDate.getTime() / 1000);
    if (cylinder.ruptura  === cylinderRupTimeStamp) {
      this.cylinderRefsList[this.editedCylinderIndex] = cylinder;
      this.cylinderTable[this.editedCylinderIndex] = cylinderTable;
      this.serviceStorage.setCylinderArray(this.cylinderRefsList);
      this.modalReference.close();
    }else {
      const colDate = new Date(this.cylinder.colado * 1000);
      const diff = Math.abs(colDate.getTime() - cylinderRupDate.getTime());
      const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
      cylinder.edad = diffDays;
      cylinder.ruptura = cylinderRupTimeStamp;
      cylinderTable.ruptura = cylinderRupDate.toDateString();
      cylinderTable.edad = diffDays;
      this.cylinderRefsList[this.editedCylinderIndex] = cylinder;
      this.cylinderTable[this.editedCylinderIndex] = cylinderTable;
      this.serviceStorage.setCylinderArray(this.cylinderRefsList);
      this.modalReference.close();
    }
  }
  saveEditedBeam() {
    const beam = JSON.parse(this.serviceStorage.getBeamArray())[this.editedBeamIndex];
    const beamTable = this.beamTable[this.editedBeamIndex];
    beam.rev = this.beamForm.get('rev').value;
    beam.mrProy = this.beamForm.get('mrProy').value;
    beam.ubicacion = this.beamForm.get('ubicacion').value;
    beamTable.rev = this.beamForm.get('rev').value;
    beamTable.mrProy = this.beamForm.get('mrProy').value;
    beamTable.ubicacion = this.beamForm.get('ubicacion').value;
    const beamRupDate = new Date(this.editedRupModel.year, this.editedRupModel.month - 1, this.editedRupModel.day);
    const beamRupTimeStamp = Math.round(beamRupDate.getTime() / 1000);
    if (beam.ruptura  === beamRupTimeStamp) {
      this.beamRefsList[this.editedBeamIndex] = beam;
      this.beamTable[this.editedBeamIndex] = beamTable;
      this.serviceStorage.setBeamArray(this.beamRefsList);
      this.modalReference.close();
    }else {
      const colDate = new Date(this.beam.colado * 1000);
      const diff = Math.abs(colDate.getTime() - beamRupDate.getTime());
      const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
      beam.edad = diffDays;
      beam.ruptura = beamRupTimeStamp;
      beamTable.ruptura = beamRupDate.toDateString();
      beamTable.edad = diffDays;
      this.beamRefsList[this.editedBeamIndex] = beam;
      this.beamTable[this.editedBeamIndex] = beamTable;
      this.serviceStorage.setBeamArray(this.beamRefsList);
      this.modalReference.close();
    }
  }
  saveEditedCube() {
    const cube = JSON.parse(this.serviceStorage.getCubeArray())[this.editedCubeIndex];
    const cubeTable = this.cubeTable[this.editedCubeIndex];
    cube.rev = this.cubeForm.get('rev').value;
    cube.resistenciaProy = this.cubeForm.get('resistenciaProy').value;
    cube.ubicacion = this.cubeForm.get('ubicacion').value;
    cubeTable.rev = this.cubeForm.get('rev').value;
    cubeTable.resistenciaProy = this.cubeForm.get('resistenciaProy').value;
    cubeTable.ubicacion = this.cubeForm.get('ubicacion').value;
    const cubeRupDate = new Date(this.editedRupModel.year, this.editedRupModel.month - 1, this.editedRupModel.day);
    const cubeRupTimeStamp = Math.round(cubeRupDate.getTime() / 1000);
    if (cube.ruptura  === cubeRupTimeStamp) {
      this.cubeRefsList[this.editedCubeIndex] = cube;
      this.cubeTable[this.editedCubeIndex] = cubeTable;
      this.serviceStorage.setCubeArray(this.cubeRefsList);
      this.modalReference.close();
    }else {
      const colDate = new Date(this.cube.colado * 1000);
      const diff = Math.abs(colDate.getTime() - cubeRupDate.getTime());
      const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
      cube.edad = diffDays;
      cube.ruptura = cubeRupTimeStamp;
      cubeTable.ruptura = cubeRupDate.toDateString();
      cubeTable.edad = diffDays;
      this.cubeRefsList[this.editedCubeIndex] = cube;
      this.cubeTable[this.editedCubeIndex] = cubeTable;
      this.serviceStorage.setCubeArray(this.cubeRefsList);
      this.modalReference.close();
    }
  }
   // Methods to delete an item from the table
   deleteCylinder(i: number) {
    if (confirm('Seguro que desea borrar el cilindro?') === true) {
      this.cylinderTable.splice(i, 1);
      this.cylinderRefsList.splice(i, 1);
      this.serviceStorage.setCylinderArray(this.cylinderRefsList);
      if (this.cylinderRefsList.length === 0) {
        this.beamTab = false;
        this.cubeTab = false;
        this.serviceStorage.deleteCylinderArray();
        this.timeStampServOrder = null;
      }
   } else {}
  }

  deleteBeam(i: number) {
    if (confirm('Seguro que desea borrar la viga?') === true) {
      this.beamTable.splice(i, 1);
      this.beamRefsList.splice(i, 1);
      this.serviceStorage.setBeamArray(this.beamRefsList);
      if (this.beamRefsList.length === 0) {
        this.cylinderTab = false;
        this.cubeTab = false;
        this.serviceStorage.deleteBeamArray();
        this.timeStampServOrder = null;
      }
   } else {}
  }

  deleteCube(i: number) {
    if (confirm('Seguro que desea borrar el cubo?') === true) {
      this.cubeTable.splice(i, 1);
      this.cubeRefsList.splice(i, 1);
      this.serviceStorage.setCubeArray(this.cubeRefsList);
      if (this.cubeRefsList.length === 0) {
        this.cylinderTab = false;
        this.beamTab = false;
        this.serviceStorage.deleteCubeArray();
        this.timeStampServOrder = null;
      }
   } else {}
  }
  // save the service order
  saveOrder() {
    if (confirm('Se guardará la orden, ¿los datos son correctos?') === true) {
      this.isFirstCylinderAdd = true;
      const cylinderRefsList = JSON.parse(this.serviceStorage.getCylinderArray());
      const beamRefsList = JSON.parse(this.serviceStorage.getBeamArray());
      const cubeRefsList = JSON.parse(this.serviceStorage.getCubeArray());
      console.log(cylinderRefsList);
      console.log(beamRefsList);
      console.log(cubeRefsList);

        this.serviceOrderService.postServiceOrder(this.timeStampServOrder, this.websafeKey, cylinderRefsList,
          beamRefsList, cubeRefsList, this.usersafeKey).subscribe(data => {
            this.serviceStorage.deleteServiceOrderInfo();
            console.log(data);

            if (this.role === 'Admin' || this.role === 'Director') {
              this.router.navigate(['/home/service-order']);
            } else if (this.role === 'Tecnico') {
              this.router.navigate(['/home-tecnician/service-order']);
            } else if (this.role === 'Supervisor') {
              this.router.navigate(['/home-supervisor/service-order']);
            }

          });
    } else { }
  }

  cancelOrder() {
    if (confirm('¿Seguro deseas cancelar la orden?') === true) {
      if (this.role === 'Admin' || this.role === 'Director') {
        this.router.navigate(['/home/service-order']);
      } else if (this.role === 'Tecnico') {
        this.router.navigate(['/home-tecnician/service-order']);
      } else if (this.role === 'Supervisor') {
        this.router.navigate(['/home-supervisor/service-order']);
      }
  } else {}
  }

  testOrder() {
    console.log(JSON.parse(this.serviceStorage.getCylinderArray()));
    console.log(this.cylinderTable);
    console.log(JSON.parse(this.serviceStorage.getBeamArray()));
    console.log(this.beamTable);
    console.log(JSON.parse(this.serviceStorage.getCubeArray()));
    console.log(this.cubeTable);
    console.log(this.timeStampServOrder);
  }
}
