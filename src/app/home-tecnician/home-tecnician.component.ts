import { Component, OnInit } from '@angular/core';
import { ServiceStorageService } from './../_storage/service-storage.service';

import { UserService } from './../_services/user.service';

@Component({
  selector: 'app-home-tecnician',
  templateUrl: './home-tecnician.component.html',
  styleUrls: ['./home-tecnician.component.css']
})
export class HomeTecnicianComponent implements OnInit {

  userName;

  constructor(
    private serviceStorage: ServiceStorageService
  ) {}

   ngOnInit() {
     const user = JSON.parse(this.serviceStorage.getUserSession());
     console.log(user);
     this.userName = user.name.toUpperCase();
   }

}
