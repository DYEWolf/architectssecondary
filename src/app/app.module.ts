import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { Http, RequestOptions } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AddServiceOrderComponent } from './service-order/add-service-order/add-service-order.component';
import { EditServiceOrderComponent } from './service-order/edit-service-order/edit-service-order.component';
import { ServiceOrderMenuComponent } from './service-order/service-order-menu/service-order-menu.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { UsersMenuComponent } from './user/users-menu/users-menu.component';
import { AddStructureComponent } from './structure/add-structure/add-structure.component';
import { StructureMenuComponent } from './structure/structure-menu/structure-menu.component';
import { EditStructureComponent } from './structure/edit-structure/edit-structure.component';
import { AddClientComponent } from './clients/add-client/add-client.component';
import { EditClientComponent } from './clients/edit-client/edit-client.component';
import { ClientsMenuComponent } from './clients/clients-menu/clients-menu.component';
import { HomeDirectorComponent } from './home-director/home-director.component';
import { HomeTecnicianComponent } from './home-tecnician/home-tecnician.component';
import { HomeSupervisorComponent } from './home-supervisor/home-supervisor.component';
import { ApproveSuperComponent } from './approver/approve-super/approve-super.component';
import { ApproveDirectorComponent } from './approver/approve-director/approve-director.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './_guards/auth.guard';
import { AuthService } from './_services/auth.service';
import { UserService } from './_services/user.service';
import { ClientService } from './_services/clients.service';
import { StructureService } from './_services/structures.service';
import { TokenInterceptor } from './_interceptor/token.interceptor';
import { ServiceOrderService } from './_services/service-order.service';
import { ServiceStorageService } from './_storage/service-storage.service';

import { routing } from './app.routing';
import { Routes, RouterModule } from '@angular/router';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Ng4FilesModule } from 'angular4-files-upload';
import { ImageUploadModule } from 'angular2-image-upload';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { DeleteDirectorComponent } from './delete-director/delete-director.component';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddUserComponent,
    AddStructureComponent,
    AddClientComponent,
    UsersMenuComponent,
    ClientsMenuComponent,
    StructureMenuComponent,
    EditUserComponent,
    EditClientComponent,
    EditStructureComponent,
    AddServiceOrderComponent,
    ServiceOrderMenuComponent,
    HomeDirectorComponent,
    HomeTecnicianComponent,
    HomeSupervisorComponent,
    EditServiceOrderComponent,
    ApproveSuperComponent,
    ApproveDirectorComponent,
    DeleteDirectorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    routing,
    HttpClientModule,
    ReactiveFormsModule,
    Ng4FilesModule,
    NgbModule.forRoot(),
    ImageUploadModule.forRoot(),
    Ng2ImgMaxModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  {
    provide: AuthHttp,
    useFactory: authHttpServiceFactory,
    deps: [Http, RequestOptions]
  },
  {provide: LocationStrategy, useClass: HashLocationStrategy},
  AuthGuard,
  AuthService,
  UserService,
  ClientService,
  StructureService,
  ServiceOrderService,
  ServiceStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
