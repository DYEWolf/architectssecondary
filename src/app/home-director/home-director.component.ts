import { ServiceStorageService } from './../_storage/service-storage.service';
import { Component, OnInit } from '@angular/core';

import { UserService } from './../_services/user.service';

@Component({
  selector: 'app-home-director',
  templateUrl: './home-director.component.html',
  styleUrls: ['./home-director.component.css']
})
export class HomeDirectorComponent implements OnInit {

  userName;

  constructor(
    private serviceStorage: ServiceStorageService
  ) {
    const user = JSON.parse(this.serviceStorage.getUserSession());
    console.log(user);
   this.userName = user.name.toUpperCase();
  }

   ngOnInit() {

   }

}
