import { Component, OnInit } from '@angular/core';

import { ClientService } from '../_services/clients.service';
import { StructureService } from '../_services/structures.service';



@Component({
  selector: 'app-delete-director',
  templateUrl: './delete-director.component.html',
  styleUrls: ['./delete-director.component.css']
})

export class DeleteDirectorComponent implements OnInit {

  currentJustify = 'fill';
  clients: any = [];
  structures: any = [];

  constructor(
    private clientService: ClientService,
    private structureService: StructureService
  ) { }

  ngOnInit() {
    this.clientService.getDeletedClients().subscribe(res => {
      this.clients = res;
    });
    this.structureService.getDeletedStructures().subscribe(res => {
      this.structures = res;
    });
  }

  approveDeleteClient(i: number) {
    const suspend = false;
    const websafeKey = this.clients.items[i].websafeKey;
    console.log(websafeKey);
    this.clientService.deleteClient(websafeKey, suspend).subscribe(res => {
      location.reload();
    });
  }

  cancelDeleteClient(i: number) {
    const client = this.clients.items[i];
    const websafeKey = this.clients.items[i].websafeKey;
    this.clientService.approveClient(websafeKey, client).subscribe(res => {
      location.reload();
    });
  }

  approveDeletedStructure(i: number) {
    const suspend = false;
    const websafeKey = this.structures.items[i].websafeKey;
    this.structureService.deleteStructure(websafeKey, suspend).subscribe(res => {
      location.reload();
    });
  }

}
