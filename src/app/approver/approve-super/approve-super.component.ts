import { Component, OnInit } from '@angular/core';
import { ServiceOrderService } from '../../_services/service-order.service';

@Component({
  selector: 'app-approve-super',
  templateUrl: './approve-super.component.html',
  styleUrls: ['./approve-super.component.css']
})
export class ApproveSuperComponent implements OnInit {

  currentJustify = 'fill';
  servOrder: any = [];

  constructor(
    private serviceOrder: ServiceOrderService
  ) { }

  ngOnInit() {
    const statusProcess = 'En Proceso';
    this.servOrder = this.serviceOrder.getServiceOrderListSup().subscribe(res => {
      this.servOrder = res;
    });
  }

  approveServiceOrder(i: number) {
    const client = this.servOrder.items[i];
    const websafeKey = this.servOrder.items[i].websafeKey;
    this.serviceOrder.approveServiceOrder(websafeKey, client).subscribe(res => {
     location.reload();
    });
  }

  cancelServiceOrder(i: number) {
    const suspend = false;
    const websafeKey = this.servOrder.items[i].websafeKey;
    this.serviceOrder.deleteServiceOrder(websafeKey, suspend).subscribe(res => {
      location.reload();
    });
  }

}
