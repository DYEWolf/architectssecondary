import { ServiceOrderService } from './../../_services/service-order.service';
import { ClientService } from './../../_services/clients.service';
import { Component, OnInit } from '@angular/core';
import { StructureService } from '../../_services/structures.service';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-approve-director',
  templateUrl: './approve-director.component.html',
  styleUrls: ['./approve-director.component.css']
})
export class ApproveDirectorComponent implements OnInit {

  currentJustify = 'fill';
  clients: any = [];
  structures: any = [];
  serviceOrders: any = [];
  modalReference: any;


  constructor(
    private clientService: ClientService,
    private structureService: StructureService,
    private serviceOrder: ServiceOrderService,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.clientService.getClientsApprove().subscribe(data => {
      this.clients = data;
    });
    this.structureService.getClientsApprove().subscribe(data => {
      this.structures = data;
    });
    this.serviceOrder.getServiceOrderApproves().subscribe(data => {
      this.serviceOrders = data;
    });
  }

  approveClient(i: number) {

    const client = this.clients.items[i];
    const websafeKey = this.clients.items[i].websafeKey;
    this.clientService.approveClient(websafeKey, client).subscribe(res => {

      location.reload();
    });
  }

  approveStructure(i: number) {

    const client = this.structures.items[i];
    const websafeKey = this.structures.items[i].websafeKey;
    this.structureService.approveStructure(websafeKey, client).subscribe(res => {

      location.reload();
    });
  }

  approveServiceOrder(i: number) {

    const client = this.serviceOrders.items[i];
    const websafeKey = this.serviceOrders.items[i].websafeKey;
    this.serviceOrder.approveServiceOrder(websafeKey, client).subscribe(res => {

      location.reload();
    });
  }

  cancelClient(i: number) {

    const suspend = false;
    const websafeKey = this.clients.items[i].websafeKey;
    this.clientService.deleteClient(websafeKey, suspend).subscribe(res => {

      location.reload();
    });
  }

  cancelStructure(i: number) {

    const suspend = false;
    const websafeKey = this.structures.items[i].websafeKey;
    this.structureService.deleteStructure(websafeKey, suspend).subscribe(res => {

      location.reload();
    });
  }

  cancelServiceOrder(i: number) {

    const suspend = false;
    const websafeKey = this.serviceOrders.items[i].websafeKey;
    this.serviceOrder.deleteServiceOrder(websafeKey, suspend).subscribe(res => {

      location.reload();
    });
  }

  more(seeMore, d: number) {
    this.modalReference = this.modalService.open(seeMore);
  }

}
