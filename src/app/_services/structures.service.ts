import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class StructureService {

  constructor(private http: HttpClient) {}

// Get all the structures
  getAllStructures() {
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/constructions/query';
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getNextPageTokenStructures(nextPageToken: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/constructions/query?nextPageToken=' + nextPageToken;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  postStructure(structure: any, usersafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/construction?usersafeKey=' + usersafeKey;
      return this.http.post(url, structure).map((response: Response) => {
      const resToJSON = JSON.stringify(response);
      return resToJSON;
    });
  }


  getAllSupervisorStructures(supervisorsafeKey: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/constructions/query?dataStatus=' + dataStatus +
    '&supervisorsafeKey=' + supervisorsafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getClientsApprove() {
    const dataStatus = 'D_APROBE_MAN';
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/constructions/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getDeletedStructures() {
    const dataStatus = 'D_APROBE_DELETE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/constructions/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  deleteStructure(webSafeKey: string, suspend: boolean) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/construction?websafeKey=' + webSafeKey +
                  '&suspend=' + suspend;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  editStructure(structure: any, websafeKey: string, usersafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/construction?websafeKey=' + websafeKey +
    '&usersafeKey=' + usersafeKey;
     return this.http.put(url, structure).map((response: Response) => {
      return response;
    });
  }

  setWebsafeKey(websafeKey: any) {
    localStorage.setItem('websafeKey', websafeKey);
  }

  getWebsafeKey() {
    return localStorage.getItem('websafeKey');
  }

  getSingleStructure(webSafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/construction/v1/construction?websafeKey=' + webSafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  approveStructure(websafeKey: string, structure: any) {
    const type = 'DATA_CONSTRUCTION';
    const url = 'https://grupo-sci.appspot.com/_ah/api/aprobe/v1/aprobe?websafeKey=' + websafeKey + '&type=' + type;
    return this.http.post(url, structure).map((response: Response) => {
      return response;
    });
  }
}


