import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';

import 'rxjs/add/operator/map';

@Injectable()
export class ClientService {


  constructor( private http: HttpClient) { }

// Create new client
  postClient(client: any, usersafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/client?usersafeKey=' + usersafeKey;
    return this.http.post(url, client).map((response: Response) => {
    const resToJSON = JSON.stringify(response);
    return resToJSON;
    });
  }
// Get all the clients
  getAllClients() {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/clients/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getNextPageTokenClients(nextPageToken: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/clients/query?dataStatus=' + dataStatus +
    '&nextPageToken=' + nextPageToken;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getDeletedClients() {
    const dataStatus = 'D_APROBE_DELETE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/clients/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getClientsApprove() {
    const dataStatus = 'D_APROBE_MAN';
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/clients/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  deleteClient(websafeKey: string, suspend: boolean) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/client?websafeKey=' + websafeKey + '&suspend=' + suspend;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  editClient(client: any, websafeKey: string, usersafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/client?websafeKey=' + websafeKey + '&usersafeKey=' + usersafeKey;
    return this.http.put(url, client).map((response: Response) => {
      return response;
    });
  }

  setWebsafeKey(websafeKey: any) {
    localStorage.setItem('websafeKey', websafeKey);
  }

  getWebsafeKey() {
    return localStorage.getItem('websafeKey');
  }

  getSingleClient(websafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/company/v1/client?websafeKey=' + websafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  approveClient(websafeKey: string, client: any) {
    const type = 'DATA_COMPANY';
    const url = 'https://grupo-sci.appspot.com/_ah/api/aprobe/v1/aprobe?websafeKey=' + websafeKey + '&type=' + type;
    return this.http.post(url, client).map((response: Response) => {
      return response;
    });
  }

}
