import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private http: HttpClient) {}

  getAccesToken(username: string, password: string) {
    const encoded = btoa(username.concat(':' + password));
    const urlPost = 'https://grupo-sci.appspot.com/_ah/api/oauth/v1/token';
    return this.http
      .post(urlPost, null, {
        headers: new HttpHeaders()
          .set('Authorization', 'Basic ' + encoded)
          .set('Content-Type', 'application/json')
      })
      .map((response: Response) => {
        localStorage.setItem('accessToken', response['accessToken']);
        localStorage.setItem('refreshToken', response['refreshToken']);
        return ;
      });
  }

  refreshToken() {
    const urlPost = 'https://grupo-sci.appspot.com/_ah/api/oauth/v1/token';
    return this.http.get(urlPost, {
      headers: new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.getLocalRefreshToken())
      .set('Content-Type', 'application/json')
    })
      .map((response: Response) => {
        localStorage.setItem('accessToken', response['accessToken']);
        return response['accessToken'];
      });
  }

  getLocalAccessToken() {
    return localStorage.getItem('accessToken');
  }

  deleteAccessToken() {
    localStorage.removeItem('accessToken');
  }

  getLocalRefreshToken() {
    return localStorage.getItem('refreshToken');
  }

  deleteRefreshToken() {
    localStorage.removeItem('refreshToken');
  }

  isTokenExpired() {
   return this.jwtHelper.isTokenExpired(this.getLocalAccessToken());
  }

  deleteCredentials() {
    this.deleteAccessToken();
    this.deleteRefreshToken();
  }
}


