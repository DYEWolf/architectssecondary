import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {}

// Get all the users based on the role
  getAllUsersByRole(role: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/users/query?role=' + role;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }
// Get all the tecnician that belongs to a superviso by the safeKey
  getAllTecnicianSuper(supervisorsafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/users/query?supervisorsafeKey=' + supervisorsafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getUser() {
    const urlPost = 'https://grupo-sci.appspot.com/_ah/api/users/v1/user';
    return this.http.get(urlPost).map((response: Response) => {
      return response;
    });
  }

  postUser(user: any,  usersafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/user?usersafeKey=' + usersafeKey;
    return this.http.post(url, user ).map((response: Response) => {
      return response;
    });
  }

  getAllUsers() {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/users/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getNextPageTokenUsers(nextPageToken: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/users/query?dataStatus=' + dataStatus + '&nextPageToken=' + nextPageToken;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  deleteUser(websafeKey: string, suspend: boolean) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/user?websafeKey=' + websafeKey + '&suspend=' + suspend;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  editUser(user: any, websafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/user?websafeKey=' + websafeKey;
    return this.http.put(url, user).map((response: Response) => {
      return response;
    });
  }

  editUserAssignation(user: any, websafeKey: string, usersafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/user?websafeKey=' + websafeKey +
                  '&usersafeKey=' + usersafeKey;
    return this.http.put(url, user).map((response: Response) => {
      return response;
    });
  }

  getSingleUser(websafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/users/v1/user/users?websafeKey=' + websafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }
}


