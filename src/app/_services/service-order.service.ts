import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceOrderService {

  constructor(private http: HttpClient) { }


// Create new service order
  postServiceOrder(
    orderDate: number,
    constructionRef: string,
    cylinderRefsList: any,
    beamRefsList: any,
    cubeRefsList: any,
    usersafeKey: string
  ) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/service?usersafeKey=' + usersafeKey;
    const jsonObject = JSON.stringify({'orderDate': orderDate, 'constructionRef': constructionRef, 'cylinderRefsList': cylinderRefsList,
    'beamRefsList' : beamRefsList, 'cubeRefsList': cubeRefsList});
     return this.http.post(url, jsonObject).map((response: Response) => {
      const resToJSON = JSON.stringify(response);
      return resToJSON;
    });
  }
// Get the services order of a supervisor via supervisorwebsafeKey
  getServiceOrderSupervisor(supervisorsafeKey: string, status: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?status=' + status +
    '&dataStatus=' + dataStatus + '&supervisorsafeKey=' + supervisorsafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }
// Get the services order of a tecnician via tecnicianwebsafeKey
  getServiceOrderTecnician(tecniciansafeKey: string, status: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?status=' + status +
    '&dataStatus=' + dataStatus + '&technicalsafeKey=' + tecniciansafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }
// Get the services order based on the status
  getServiceOrderList(status: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?status=' + status +
                '&dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getNextPageServiceOrderAdmin(status: string, nextPageToken: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?status=' + status +
                '&dataStatus=' + dataStatus + '&nextPageToken=' + nextPageToken;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getNextPageServiceOrderSupervisor(supervisorsafeKey: string, status: string, nextPageToken: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?status=' + status +
    '&dataStatus=' + dataStatus + '&supervisorsafeKey=' + supervisorsafeKey + 'nextPageToken=' + nextPageToken;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getNextPageServiceOrderTecnician(tecniciansafeKey: string, status: string, nextPageToken: string) {
    const dataStatus = 'D_ACTIVE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?status=' + status +
    '&dataStatus=' + dataStatus + '&technicalsafeKey=' + tecniciansafeKey + '&nextPageToken=' + nextPageToken;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  editServiceOrder(
    cylinderRefsList: any,
    beamRefsList: any,
    cubeRefsList: any,
    websafeKey: string,
    usersafeKey: string,
    constructionRef: string,
    orderDate: number) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/service?websafeKey=' + websafeKey + '&usersafeKey=' + usersafeKey;
    const jsonObject = JSON.stringify({'orderDate': orderDate, 'constructionRef': constructionRef, 'cylinderRefsList': cylinderRefsList,
    'beamRefsList' : beamRefsList, 'cubeRefsList': cubeRefsList});
    return this.http.put(url, jsonObject).map((response: Response) => {
      return response;
    });

  }

  getServiceOrder(websafeKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/service?websafeKey=' + websafeKey;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }



  getServiceOrderApproves() {
    const dataStatus = 'D_APROBE_MAN';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }

  getServiceOrderListSup() {
    const dataStatus = 'D_APROBE_SUP';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/services/query?dataStatus=' + dataStatus;
    return this.http.get(url).map((response: Response) => {
      return response;
    });
  }



  deleteServiceOrder(websafeKey: string, suspend: boolean) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/service?websafeKey=' + websafeKey +
          '&suspend=' + suspend;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  postNewCylinder(serviceKey: string, cylinder: any) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/cylinder/v1/cylinder?serviceKey=' + serviceKey;
    const jsonObject = JSON.stringify(cylinder);
    return this.http.post(url, jsonObject).map((response: Response) => {
      return response;
    });
  }

  postNewBeam(serviceKey: string, beam: any) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/beam/v1/beam?serviceKey=' + serviceKey;
    const jsonObject = JSON.stringify(beam);
    return this.http.post(url, jsonObject).map((response: Response) => {
      return response;
    });
  }

  postNewCube(serviceKey: string, cube: any) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/cube/v1/cube?serviceKey=' + serviceKey;
    const jsonObject = JSON.stringify(cube);
    return this.http.post(url, jsonObject).map((response: Response) => {
      return response;
    });
  }

  editCylinder(websafeKey: string, cylinder: any) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/cylinder/v1/cylinder?websafeKey=' + websafeKey;
    const jsonObject = JSON.stringify(cylinder);
    return this.http.put(url, jsonObject).map((response: Response) => {
      return response;
    });
  }

  editBeam(websafeKey: string, beam: any) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/beam/v1/beam?websafeKey=' + websafeKey;
    const jsonObject = JSON.stringify(beam);
    return this.http.put(url, jsonObject).map((response: Response) => {
      return response;
    });
  }

  editCube(websafeKey: string, cube: any) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/cube/v1/cube?websafeKey=' + websafeKey;
    const jsonObject = JSON.stringify(cube);
    return this.http.put(url, jsonObject).map((response: Response) => {
      return response;
    });
  }

  deleteCylinder(websafeKey: string, serviceKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/cylinder/v1/cylinder?websafeKey=' + websafeKey + '&serviceKey=' + serviceKey;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  deleteBeam(websafeKey: string, serviceKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/beam/v1/beam?websafeKey=' + websafeKey + '&serviceKey=' + serviceKey;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  deleteCube(websafeKey: string, serviceKey: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/cube/v1/cube?websafeKey=' + websafeKey + '&serviceKey=' + serviceKey;
    return this.http.delete(url).map((response: Response) => {
      return response;
    });
  }

  approveServiceOrder(websafeKey: string, serviceOrder: any) {
    const type = 'DATA_SERVICE';
    const url = 'https://grupo-sci.appspot.com/_ah/api/aprobe/v1/aprobe?websafeKey=' + websafeKey + '&type=' + type;
    return this.http.post(url, serviceOrder).map((response: Response) => {
      return response;
    });
  }

  finishServiceOrder(websafeKey: string) {
    const status = 'Terminado';
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/service?websafeKey=' + websafeKey + '&status=' + status;
    return this.http.put(url, null).map((response: Response) => {
      return response;
    });
  }

  updateRuptura(websafeKey: string, orderDate: string) {
    const url = 'https://grupo-sci.appspot.com/_ah/api/service/v1/service?websafeKey=' + websafeKey + '&orderDate=' + orderDate;
    return this.http.put(url, null).map((response: Response) => {
      return response;
    });
  }
}
