import { UserService } from './../../_services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ServiceStorageService } from '../../_storage/service-storage.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-users-menu',
  templateUrl: './users-menu.component.html',
  styleUrls: ['./users-menu.component.css']
})
export class UsersMenuComponent implements OnInit {

users: any = [];
modalReference: any;
delete = false;
websafeKey: string;
userName: string;
isNextPage: number;
nextPageToken: string;

  constructor(
    private userService: UserService,
    private router: Router,
    private serviceStorage: ServiceStorageService,
    private modalService: NgbModal,
  ) {}

  ngOnInit() {
    this.userService.getAllUsers().subscribe(data => {
      const usersArray = data;
      this.users = usersArray['items'].filter(
        user => user.role !== 'Admin');
      this.nextPageToken = data['nextPageToken'];
      this.isNextPage = this.users.length;
    });
  }

  deleteUser() {
    if (this.delete === false) {
      const suspend = true;
      this.userService.deleteUser(this.websafeKey, suspend).subscribe(res => {
        this.modalReference.close();
        location.reload();
      });
    } else {
      const suspend = false;
      this.userService.deleteUser(this.websafeKey, suspend).subscribe(res => {
        this.modalReference.close();
        location.reload();
      });
    }
  }

  editUser(i: number) {
    const websafeKey = this.users[i].websafeKey;
    this.userService.getSingleUser(websafeKey).subscribe(res => {
      this.serviceStorage.setSingleUser(res);
      this.router.navigate(['/home/users/edit']);
    });
  }

  moreUsers() {
    this.userService.getNextPageTokenUsers(this.nextPageToken).subscribe(res => {
      this.nextPageToken = res['nextPageToken'];
      if (!res['items']) {
        alert('No hay mas elementos que mostrar');
      } else {
        this.users = this.users.concat(res['items']);
      }
    });
  }

  add() {
    this.router.navigate(['/home/users/add']);
  }

  open(deleteModal, i: number) {
    this.modalReference = this.modalService.open(deleteModal, {size: 'lg'});
    this.websafeKey = this.users[i].websafeKey;
    this.userName = this.users[i].name;
    console.log(this.userName);
  }
}
