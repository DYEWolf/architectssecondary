import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Ng2ImgMaxService } from 'ng2-img-max';

import { UserService } from './../../_services/user.service';
import { FileHolder } from 'angular2-image-upload';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  errMessage: string;
  employeeForm: FormGroup;
  imageProfile = null;
  users: any = [];
  userName = 'Seleccione un rol primero';
  usersafeKey: string;

  constructor(
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder,
    private ng2ImgMaxService: Ng2ImgMaxService,
  ) {
  }

  ngOnInit() {
    this.employeeForm = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
      role: new FormControl('', Validators.required)
    });
    this.onChangesUser();
  }

  onChangesUser(): void {
    let role;
    this.employeeForm.get('role').valueChanges.subscribe(val => {
      console.log(val);
      if (val === 'Tecnico') {
        role = 'Supervisor';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
      } else if (val === 'Supervisor') {
        role = 'Director';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
      } else if (val === 'Director') {
        role = 'Admin';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
      }
    });
  }

  confirmPassword() {
    if (this.employeeForm.get('password').value === this.employeeForm.get('confirmPassword').value) {
      return true;
    } else {
      this.errMessage = 'La contraseña no coincide';
      return false;
    }
  }

  save() {
    const matchPassword = this.confirmPassword();
    if (matchPassword === true && this.imageProfile !== null) {
      const employee = this.employeeForm.value;
      const email = employee.email;
      employee.email = {email};
      delete employee.confirmPassword;
      employee.profileImage = this.imageProfile.split('base64,')[1];
      this.errMessage = null;
      console.log(employee);
      this.userService.postUser(employee, this.usersafeKey).subscribe(res => {
        this.router.navigate(['/home/users']);
      }, err => {
        this.errMessage = 'Error al agregar usuario';
      });
    } else if (this.imageProfile === null) {
      this.errMessage = 'Por favor agregue una foto de perfil';
    } else if (this.userName === 'Seleccione un rol primero') {
      this.errMessage = 'Por favor asigne el empleado';
    }
  }

  cancel() {
    this.router.navigate(['/home/users']);
  }

  userInfo(name: string, usersafeKey: string) {
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }

  onImageChange(event) {
    const image = event.target.files[0];
    this.ng2ImgMaxService.resizeImage(image, 70, 70).subscribe(
      result => {
        const reader = new FileReader();
        reader.onload = e => {
          const file = reader.result;
          this.imageProfile = file;
          this.errMessage = null;
          return file;
        };
        reader.readAsDataURL(result);
      },
      error => {
        console.log('Oh no', error);
      }
    );
  }
}
