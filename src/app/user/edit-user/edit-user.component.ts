import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Employee } from './../../_models/employees-model';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import { ServiceStorageService } from '../../_storage/service-storage.service';
import { FileHolder } from 'angular2-image-upload';
import { Ng2ImgMaxService } from 'ng2-img-max';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  websafeKey: string;
  usersafeKey: string;
  errMessage: string;
  user: any;
  users: any = [];
  employeeForm: FormGroup;
  imageProfile: string;
  userName: string;

  changeImage = false;
  changeRole = false;
  imagePath;
  defaultRole: string;

  constructor(
    private userService: UserService,
    private router: Router,
    private serviceStorage: ServiceStorageService,
    private fb: FormBuilder,
    private ng2ImgMaxService: Ng2ImgMaxService,
  ) {}

  ngOnInit() {
    let role: string;
    this.user = JSON.parse(this.serviceStorage.getSingleUser());
    console.log(this.user);
    this.userName = this.user.assignedTo.name;
    this.websafeKey = this.user.websafeKey;
    this.usersafeKey = this.user.assignedTo.websafeKey;
    console.log(this.websafeKey);
    console.log(this.usersafeKey);
    this.imagePath = this.user.profileImage;
    this.employeeForm = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
      role: new FormControl('')
    });
    this.employeeForm.patchValue({
      name: this.user.name,
      email: this.user.email.email,
      userName: this.user.userName,
      password: this.user.password,
      role: this.user.role
    });
    this.defaultRole = this.user.role;
    if (this.user.role === 'Tecnico') {
      role = 'Supervisor';
      this.userService.getAllUsersByRole(role).subscribe(response => {
        this.users = response;
      });
    } else if (this.user.role === 'Supervisor') {
      role = 'Director';
      this.userService.getAllUsersByRole(role).subscribe(response => {
        this.users = response;
      });
    } else if (this.user.role === 'Director') {
      role = 'Admin';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
    }
    this.onChangesUser();
  }

  save () {
    if (this.changeImage === true && this.userName === undefined) {
      const user = this.employeeForm.value;
      const email = user.email;
      user.email = {email};
      user.profileImage = this.imageProfile.split('base64,')[1];
      delete user.confirmPassword;
      this.userService.editUser(user, this.websafeKey).subscribe(res => {
        this.router.navigate(['/home/users']);
      }, err => this.errMessage = 'Error al agregar usuario');
    } else if (this.userName !== undefined && this.changeImage === false) {
      const user = this.employeeForm.value;
      const email = user.email;
      user.email = {email};
      user.profileImage = this.user.profileImage;
      delete user.confirmPassword;
      this.userService.editUserAssignation(user, this.websafeKey, this.usersafeKey).subscribe(res => {
        this.router.navigate(['/home/users']);
      }, err => this.errMessage = 'Error al agregar usuario, intente nuevamente');
    } else if (this.userName !== undefined && this.changeImage === true) {
      const user = this.employeeForm.value;
      const email = user.email;
      user.email = {email};
      user.profileImage = this.imageProfile.split('base64,')[1];
      delete user.confirmPassword;
      this.userService.editUserAssignation(user, this.websafeKey, this.usersafeKey).subscribe(res => {
        this.router.navigate(['/home/users']);
      }, err => this.errMessage = 'Error al agregar usuario, intente nuevamente');
    } else {
      const user = this.employeeForm.value;
      const email = user.email;
      user.email = {email};
      user.profileImage = this.user.profileImage;
      delete user.confirmPassword;
      this.userService.editUser(user, this.websafeKey).subscribe(res => {
        this.router.navigate(['/home/users']);
      }, err => this.errMessage = 'Error al agregar usuario, intente nuevamente');
    }
  }

  cancel() {
    this.router.navigate(['/home/users']);
  }

  onImageChange(event) {
    this.changeImage = true;
    const image = event.target.files[0];
    this.ng2ImgMaxService.resizeImage(image, 70, 70).subscribe(
      result => {
        const reader = new FileReader();
        reader.onload = e => {
          const file = reader.result;
          this.imageProfile = file;
          return file;
        };
        reader.readAsDataURL(result);
      },
      error => {
        console.log('Oh no', error);
      }
    );
  }

  userInfo(name: string, usersafeKey: string) {
    this.userName = name;
    this.usersafeKey = usersafeKey;
  }

  onChangesUser(): void {
    let role: string;
    this.employeeForm.get('role').valueChanges.subscribe(val => {
      if (role === 'Tecnico') {
        role = 'Supervisor';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
      } else if (role === 'Supervisor') {
        role = 'Director';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
      } else if (role === 'Director') {
        role = 'Admin';
        this.userService.getAllUsersByRole(role).subscribe(response => {
          this.users = response;
        });
      }
    });
  }

}
