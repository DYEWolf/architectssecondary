import { DeleteDirectorComponent } from './delete-director/delete-director.component';
import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';

import { LoginComponent } from './login/login.component';
import { UsersMenuComponent } from './user/users-menu/users-menu.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { ClientsMenuComponent } from './clients/clients-menu/clients-menu.component';
import { AddClientComponent } from './clients/add-client/add-client.component';
import { StructureMenuComponent } from './structure/structure-menu/structure-menu.component';
import { AddStructureComponent } from './structure/add-structure/add-structure.component';
import { ServiceOrderMenuComponent } from './service-order/service-order-menu/service-order-menu.component';
import { EditStructureComponent } from './structure/edit-structure/edit-structure.component';
import { EditClientComponent } from './clients/edit-client/edit-client.component';
import { AddServiceOrderComponent } from './service-order/add-service-order/add-service-order.component';
import { HomeDirectorComponent } from './home-director/home-director.component';
import { HomeTecnicianComponent } from './home-tecnician/home-tecnician.component';
import { HomeSupervisorComponent } from './home-supervisor/home-supervisor.component';
import { EditServiceOrderComponent } from './service-order/edit-service-order/edit-service-order.component';
import { ApproveSuperComponent } from './approver/approve-super/approve-super.component';
import { ApproveDirectorComponent } from './approver/approve-director/approve-director.component';

const appRoutes: Routes = [

  // { path: '', component: ServiceOrderMenuComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/login',  pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeDirectorComponent, canActivate: [AuthGuard], children: [
   { path: 'clients', component: ClientsMenuComponent, canActivate: [AuthGuard]},
   { path: 'clients/add', component: AddClientComponent,  canActivate: [AuthGuard]},
   { path: 'clients/edit', component: EditClientComponent, canActivate: [AuthGuard] },
   { path: 'structures', component: StructureMenuComponent, canActivate: [AuthGuard]},
   { path: 'structures/add', component: AddStructureComponent, canActivate: [AuthGuard]},
   { path: 'structures/edit', component: EditStructureComponent, canActivate: [AuthGuard]},
   { path: 'users', component: UsersMenuComponent, canActivate: [AuthGuard]},
   { path: 'users/add', component: AddUserComponent, canActivate: [AuthGuard] },
   { path: 'users/edit', component: EditUserComponent, canActivate: [AuthGuard] },
   { path: 'service-order', component: ServiceOrderMenuComponent, canActivate: [AuthGuard]},
   { path: 'service-order/add', component: AddServiceOrderComponent, canActivate: [AuthGuard]},
   { path: 'service-order/edit', component: EditServiceOrderComponent, canActivate: [AuthGuard]},
   { path: 'approver', component: ApproveDirectorComponent, canActivate: [AuthGuard]},
   { path: 'deleter', component: DeleteDirectorComponent, canActivate: [AuthGuard]}
  ]},
  { path: 'home-tecnician', component: HomeTecnicianComponent, canActivate: [AuthGuard], children: [
    { path: 'service-order', component: ServiceOrderMenuComponent, canActivate: [AuthGuard]},
    { path: 'service-order/add', component: AddServiceOrderComponent, canActivate: [AuthGuard]},
    { path: 'service-order/edit', component: EditServiceOrderComponent, canActivate: [AuthGuard]}
  ]},
  { path: 'home-supervisor', component: HomeSupervisorComponent, canActivate: [AuthGuard], children: [
   { path: 'clients', component: ClientsMenuComponent, canActivate: [AuthGuard]},
   { path: 'clients/add', component: AddClientComponent,  canActivate: [AuthGuard]},
   { path: 'clients/edit', component: EditClientComponent, canActivate: [AuthGuard] },
   { path: 'structures', component: StructureMenuComponent, canActivate: [AuthGuard]},
   { path: 'structures/add', component: AddStructureComponent, canActivate: [AuthGuard]},
   { path: 'structures/edit', component: EditStructureComponent, canActivate: [AuthGuard]},
   { path: 'service-order', component: ServiceOrderMenuComponent, canActivate: [AuthGuard]},
   { path: 'service-order/add', component: AddServiceOrderComponent, canActivate: [AuthGuard]},
   { path: 'service-order/edit', component: EditServiceOrderComponent, canActivate: [AuthGuard]},
   { path: 'approver', component: ApproveSuperComponent, canActivate: [AuthGuard]}
  ]},
  { path: '**', redirectTo: 'login' }
];

// delete the enabletracing when finish, its only for debug porpouse only
export const routing = RouterModule.forRoot(appRoutes, {enableTracing: true});
